
import React from 'react'
import { ImageSourcePropType } from 'react-native'
import { Item } from 'react-native-picker-select'
import { Integrante } from './Integrante'
import { Resenia } from './Resenia'
import { ComboLista } from './ComboLista';

export interface Talento{
    "id": string,
    "talento": string,
    "descripcion":string,
    "imagen": ImageSourcePropType,
    "valoracion": number,
    "costoNivel":number,
    "popularidad":number,
    "fechaAlta":string,
    "categoria":string,
    "contratacion":{
        "integrantes":ComboLista[],
        "duracion":ComboLista[],
        "disponibilidad": number[],
        "horariosDisponibles":string[]
    },
    "resenias":Resenia[] ,
    "integrantesSeleccionado":string,
    "duracion":string,
    "costoFinal":string ,
    "costoFinalmultiplicado":string,
    "integrantesFinal":string,
    "diasSolicitados":string[],
    "horarioSeleccionado":string
}