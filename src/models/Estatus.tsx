
import React from 'react'
import { ImageSourcePropType } from 'react-native'

export interface Estatus{
    "id": number,
    "Descripcion":string,
    "estatus":number,
    "hora": string,
    "fecha":string
}
