
import React from 'react'
import { ImageSourcePropType } from 'react-native'

export interface Categoria{
    "id": string,
    "descripcion": string,
    "background": ImageSourcePropType,
}