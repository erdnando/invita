import React from 'react'

export interface Realtorio{
    filtroCliente:string,
    omitFilter:boolean,
    filtroClienteId:number,
    filtroFechaInicial:string,
    filtroFechaFinal:string,
    isFilterCollapsed:boolean,
    isSelectorParecer:boolean,
}