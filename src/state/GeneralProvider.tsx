import React from 'react'
import { Flags } from '../models/Flags';
import { TipoUsuario } from '../models/Usuario';
import { IDs } from '../models/IDs';
import { Categoria } from '../models/Categoria';
import { Talento } from '../models/Talento';
import { Estatus } from '../models/Estatus';

//https://medium.com/bam-tech/add-custom-icons-to-your-react-native-application-f039c244386c

interface GeneralState{
    flags:Flags;
    categorias:Categoria[];
    talentosMasPopulares:Talento[];
    talentosBusqueda:Talento[];
    talentosList:Talento[];
    talentoSelected:Talento;
    estatus:Estatus[];
    ids:IDs;
    tabSelected:string;
    tabSelectedOld:string;
    tabModule:string;
    setFlags:(flags:Flags)=>void;
    setIds:(ids:IDs)=>void;
    logOut: ()=>void,
    setTabSelected: (tabSelected:string)=>void;
    setTabSelectedOld: (tabSelectedOld:string)=>void;
    setTabModule: (tabModule:string)=>void;
    setCategorias:(categorias:Categoria[])=>void;
    setTalentosMasPopulares:(talentosMasPopulares:Talento[])=>void;
    setTalentosBusqueda:(talentosBusqueda:Talento[])=>void;
    setTalentosList:(talentosList:Talento[])=>void;
    setTalentoSelected:(talentoSelected:Talento)=>void;
    setEstatus:(estatus:Estatus[])=>void;
}

const GeneralContext = React.createContext({} as GeneralState);

class GeneralProvider extends React.Component{

    state = {
        tabSelected:'Logo',
        tabSelectedOld:'Logo',
        tabModule:'Logo',
        flags:{
            isLogedIn: false,
            isLoading:false,
            isLoadingParecer:false,
            isLoadingAgenda:false,
            isLoadingSearch:false,
            isLoadingContacto:false,
            isLoadingNotificaciones:true,
            isLoadingResumoOportunidades:false,
            isNotificaciones:false,
            verDetalleAgenda:false,
            resultadosBusquedaVisible:true,
            modalFiltrosVisible:false,
            modalFechaVisible:false,
            modalFechaHorarioVisible:false,
            isPasswordReseted:false,
            isDownloadingFile:0,
            existsNotification:false,
            isLoadingMonthAgenda:false,
            isLoadingResetSena:false,
            isLoadingforgotPassword:false,
            resultadosAgendaVisible:false,
            isLoadingFilterCliente:false,
            isLoadingRelatorio:false,
            isLoadingImageTalento:false,
            modalEstatusVisible:false,
            modalBuscadorVisible:false,
            modalSubCategoriaVisible:false,
            modalArtistaVisible:false,
            modalReservarVisible:false,
            modalContratarVisible:false,
            modalGuia01Visible:true,
            guiaStep1:true,
            guiaStep2:false,
            guiaStep3:false,
            guiaStep4:false,
            filtroValoracion:false,
            filtroValoracionValue:0,
            orderByMasPoulares:false,
            orderByMasRecientes:false,
            orderByMenorMayorPrecio:false,
            orderByMayorMenorPrecio:false,
            valoracion1:false,
            valoracion2:false,
            valoracion3:false,
            valoracion4:false,
            valoracion5:false,
            modalTalentoVisible:false,
            modalReseniaVisible:false
        },
        ids:{
            idOpinionBusqueda: '',
            idOpinionSeleccionado:'',
            codigoBusqueda:'',
            idMenuOpinionSelected:1,
            clienteIdSeleccionado:'',
            busquedaTalento:'',
            codigoEvento:''
        },
        categorias:[
            {
                "id": "1",
                "descripcion": "ella",
                "background": require("../assets/cat_ella.png"),  
            },
            {
                "id": "2",
                "descripcion": "infantil",
                "background": require("../assets/cat_infan.png"),
            },
            {
                "id": "3",
                "descripcion": "regional",
                "background": require("../assets/cat_regional.png"),
            },
            {
                "id": "4",
                "descripcion": "mariachi",
                "background": require("../assets/cat_mari.png"),
            },
            {
                "id": "5",
                "descripcion": "amenidades",
                "background": require("../assets/cat_amenidades.png"),
            },
            {
                "id": "6",
                "descripcion": "fiesta",
                "background": require("../assets/cat_fiesta.png"),
            },
            {
                "id": "7",
                "descripcion": "stand up",
                "background": require("../assets/cat_standup.png"),
            },
            {
                "id": "8",
                "descripcion": "versatil",
                "background": require("../assets/cat_vers.png"),
            },
            {
                "id": "9",
                "descripcion": "mariachi",
                "background": require("../assets/cat_mari.png"),
            },
            {
                "id": "10",
                "descripcion": "individuales",
                "background": require("../assets/cat_individuales.png"),
            }
        ],
        talentosMasPopulares:[],
        talentosBusqueda:[],
        talentoSelected: {
            "id": "0",
            "talento": "",
            "descripcion":"",
            "imagen": require("../assets/cat_ella.png"),  
            "valoracion": 0,  //
            "costoNivel":0,
            "popularidad":0,//OK
            "fechaAlta": '',
            "categoria":"",
            "contratacion":{
                "integrantes":[],
                "duracion":[],
                "disponibilidad":[],
                "horariosDisponibles":[]
            },
            "resenias":[] ,
            "integrantesSeleccionado":"0",
            "duracion":"0",
            "costoFinal":"0",
            "costoFinalmultiplicado":"0",
            "integrantesFinal":"0",
            "diasSolicitados":[],
            "horarioSeleccionado":""
        },
        talentosList:[
            {
                "id": "1",
                "talento": "Banda el Recodo Mexico",
                "descripcion":"Elementumx diamamed sapien id felis sodales niacer dentalious diamamed sapien id",
                "imagen": require("../assets/mariachiSol.png"),  
                "valoracion": 5,  //
                "costoNivel":4,
                "popularidad":5,//OK
                "fechaAlta": '12/07/2022',
                "categoria":"Regional",
                "contratacion":{
                    "integrantes":[
                                   {"label": "1",  "value":"1|3999.99"},
                                   {"label": "3",  "value":"3|4999.99"},
                                   {"label": "5",  "value":"5|6999.99"},
                                   {"label": "11", "value":"11|9999.99"}],
                    "duracion":[
                        {"label": "1", "value":1},
                        {"label": "2", "value":2},
                        {"label": "3", "value":3},
                        {"label": "4", "value":4},
                        {"label": "5", "value":5},
                    ],
                    "disponibilidad":[4,5,6,7],
                    "horariosDisponibles":[
                        "15:00",
                        "16:00",
                        "17:00",
                        "18:00",
                        "19:00",
                        "20:00",
                        "21:00",
                        "22:00",
                        "23:00",
                        "24:00",
                        "01:00",
                        "02:00",
                        "03:00",
                        "04:00"
                    ]
                },
                "resenias":[
                    {
                        "cliente":"Juan Carlos",
                        "fecha": "04/07/2022",
                        "resenia":"El servicio bastante bueno y la musica, excelente!. El grupo es altamente recomendable, Les recomiendo contratar 5 dias antes del evento",
                        "calificacion":5
                    },
                    {
                        "cliente":"Andrea Jimenes",
                        "fecha": "12/05/2022",
                        "resenia":"bastante puntuales, amplia variedad!!!!. En verdad los ame y mas cuando empezo el show de luces y que decir de su equipo de sonido. Delos mejores",
                        "calificacion":4
                    },
                    {
                        "cliente":"Luis torres",
                        "fecha": "23/02/2022",
                        "resenia":"Precios bastante accesibles, me encantaron!!",
                        "calificacion":5
                    }
                ],
                "integrantesSeleccionado":"0",
                "duracion":"0",
                "costoFinal":"0",
                "costoFinalmultiplicado":"0",
                "integrantesFinal":"0",
                "diasSolicitados":[],
                "horarioSeleccionado":""
            },
            {
                "id": "2",
                "talento": "Mariachis el Sol de Mexico",
                "descripcion":"Con mas de 5 años de experiencia brindando la mejor musica regional para todo su publico. ¡Banda Mariachis el Sol, un exito para ti!",
                "imagen": require("../assets/mariachiSol.png"),  
                "valoracion": 4,
                "costoNivel":3,
                "popularidad":4,
                "fechaAlta": '11/07/2022',
                "categoria":"Mariachi",
                "contratacion":{
                    "integrantes":[
                                   {"label": "1", "value":"1|2999.99"},
                                   {"label": "3", "value":"3|4999.99"}
                                ],
                    "duracion":[ 
                        {"label":"1", "value":1},
                        {"label": "2", "value":2},
                        {"label": "3", "value":3},
                        {"label": "4", "value":4},
                        {"label": "5", "value":5},
                    ],
                    "disponibilidad":[4,5,6,7],
                    "horariosDisponibles":[
                        "15:00",
                        "16:00",
                        "17:00",
                        "18:00",
                        "19:00",
                        "20:00",
                        "21:00",
                        "22:00",
                        "23:00",
                        "24:00",
                        "01:00",
                        "02:00",
                        "03:00",
                        "04:00"
                    ]
                },
                "resenias":[
                    {
                        "cliente":"Juan Carlos",
                        "fecha": "04/07/2022",
                        "resenia":"el servicio bastante bueno y la musica, excelente!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Andrea Jimenes",
                        "fecha": "12/05/2022",
                        "resenia":"bastante puntuales, amplia variedad!!!!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Luis torres",
                        "fecha": "23/02/2022",
                        "resenia":"Precios bastante accesibles, me encantaron!!",
                        "calificacion":5
                    }
                ],
                "integrantesSeleccionado":"0",
                "duracion":"0",
                "costoFinal":"0",
                "costoFinalmultiplicado":"0",
                "integrantesFinal":"0",
                "diasSolicitados":[],
                "horarioSeleccionado":""
            },
            {
                "id": "3",
                "talento": "Grupo Mar y Arena",
                "descripcion":"Elementum diamamed sapien id felis sodales",
                "imagen": require("../assets/mariachiSol.png"),  
                "valoracion": 5,
                "costoNivel":2,
                "popularidad":3,
                "fechaAlta": '01/07/2022',
                "categoria":"Ella",
                "contratacion":{
                    "integrantes":[
                                   {"label": "1", "value":"1|650.99"},
                                   {"label": "3", "value":"3|999.99"},
                                ],
                    "duracion":[
                        {"label": "1", "value":1},
                        {"label": "2", "value":2},
                        {"label": "3", "value":3},
                        {"label": "4", "value":4},
                        {"label": "5", "value":5},
                    ],
                    "disponibilidad":[5,6,7],
                    "horariosDisponibles":[
                        "15:00",
                        "16:00",
                        "17:00",
                        "18:00",
                        "19:00",
                        "20:00",
                        "21:00",
                        "22:00",
                        "23:00",
                        "24:00",
                        "01:00",
                        "02:00",
                        "03:00",
                        "04:00"
                    ]
                },
                "resenias":[
                    {
                        "cliente":"Juan Carlos",
                        "fecha": "04/07/2022",
                        "resenia":"el servicio bastante bueno y la musica, excelente!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Andrea Jimenes",
                        "fecha": "12/05/2022",
                        "resenia":"bastante puntuales, amplia variedad!!!!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Luis torres",
                        "fecha": "23/02/2022",
                        "resenia":"Precios bastante accesibles, me encantaron!!",
                        "calificacion":5
                    }
                ],
                "integrantesSeleccionado":"0",
                "duracion":"0",
                "costoFinal":"0", 
                "costoFinalmultiplicado":"0",
                "integrantesFinal":"0",
                "diasSolicitados":[],
                "horarioSeleccionado":""
            },
            {
                "id": "4",
                "talento": "Victor Arreola",
                "descripcion":"Elementum diamamed sapien id felis sodales",
                "imagen": require("../assets/mariachiSol.png"),  
                "valoracion": 5,
                "costoNivel":1,
                "popularidad":2,
                "fechaAlta": '12/06/2022',
                "categoria":"Stand up",
                "contratacion":{
                    "integrantes":[
                                   {"label": "1", "value":"1|600"},
                                   {"label": "3", "value":"3|900"},
                                ],
                    "duracion":[
                        {"label":"1", "value":1},
                        {"label": "2", "value":2},
                        {"label": "3", "value":3},
                        {"label": "4", "value":4},
                        {"label": "5", "value":5},
                    ],
                    "disponibilidad":[5,6,7],
                    "horariosDisponibles":[
                        "15:00",
                        "16:00",
                        "17:00",
                        "18:00",
                        "19:00",
                        "20:00",
                        "21:00",
                        "22:00",
                        "23:00",
                        "24:00",
                        "01:00",
                        "02:00",
                        "03:00",
                        "04:00"
                    ]
                },
                "resenias":[
                    {
                        "cliente":"Juan Carlos",
                        "fecha": "04/07/2022",
                        "resenia":"el servicio bastante bueno y la musica, excelente!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Andrea Jimenes",
                        "fecha": "12/05/2022",
                        "resenia":"bastante puntuales, amplia variedad!!!!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Luis torres",
                        "fecha": "23/02/2022",
                        "resenia":"Precios bastante accesibles, me encantaron!!",
                        "calificacion":5
                    }
                ],
                "integrantesSeleccionado":"0",
                "duracion":"0",
                "costoFinal":"0",
                "costoFinalmultiplicado":"0",
                "integrantesFinal":"0",
                "diasSolicitados":[],
                "horarioSeleccionado":""
            },
            {
                "id": "5",
                "talento": "Mago Roy",
                "descripcion":"Elementum diamamed sapien id felis sodales",
                "imagen": require("../assets/mariachiSol.png"),  
                "valoracion": 4,
                "costoNivel":1,
                "popularidad":1,
                "fechaAlta": '12/01/2022',
                "categoria":"Infantil",
                "contratacion":{
                    "integrantes":[
                                   {"label": "1", "value":"1|3999.99"},
                                   {"label": "2", "value":"2|4999.99"},
    
                                ],
                    "duracion":[
                        {"label":"1", "value":1},
                        {"label": "2", "value":2},
                        {"label": "3", "value":3},
                        {"label": "4", "value":4},
                        {"label": "5", "value":5},
                    ],
                    "disponibilidad":[5,6,7],
                    "horariosDisponibles":[
                        "15:00",
                        "16:00",
                        "17:00",
                        "18:00",
                        "19:00",
                        "20:00",
                        "21:00",
                        "22:00",
                        "23:00",
                        "24:00",
                        "01:00",
                        "02:00",
                        "03:00",
                        "04:00"
                    ]
                },
                "resenias":[
                    {
                        "cliente":"Juan Carlos",
                        "fecha": "04/07/2022",
                        "resenia":"el servicio bastante bueno y la musica, excelente!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Andrea Jimenes",
                        "fecha": "12/05/2022",
                        "resenia":"bastante puntuales, amplia variedad!!!!",
                        "calificacion":5
                    },
                    {
                        "cliente":"Luis torres",
                        "fecha": "23/02/2022",
                        "resenia":"Precios bastante accesibles, me encantaron!!",
                        "calificacion":5
                    }
                ],
                "integrantesSeleccionado":"0",
                "duracion":"0",
                "costoFinal":"0",
                "costoFinalmultiplicado":"0",
                "integrantesFinal":"0",
                "diasSolicitados":[],
                "horarioSeleccionado":""
            }
        ],
        estatus:[
            {
                "id": 1,
                "Descripcion": "Tu talento ha sido confirmado",
                "estatus":1,
                "hora": '11:00',
                "fecha":'05/07/2022'
            },
            {
                "id": 2,
                "Descripcion": "Hemos recibido tu pago",
                "estatus":0,
                "hora": '11:00',
                "fecha":'05/07/2022'
            },
            {
                "id": 3,
                "Descripcion": "El talento va en camino",
                "estatus":0,
                "hora": '11:00',
                "fecha":'05/07/2022'
            },
            {
                "id": 4,
                "Descripcion": "Finalizado",
                "estatus":0,
                "hora": '11:00',
                "fecha":'05/07/2022'
            },
            {
                "id": 5,
                "Descripcion": "Cancelado",
                "estatus":0,
                "hora": '11:00',
                "fecha":'05/07/2022'
            }
        ],
    }

    setFlags = (flags:Flags)=> this.setState({flags})
    setIds = (ids:IDs)=> this.setState({ids})
    setTabSelected = (tabSelected:string) => this.setState({tabSelected})
    setTabSelectedOld = (tabSelectedOld:string) => this.setState({tabSelectedOld})
    setTabModule = (tabModule:string) => this.setState({tabModule})
    setCategorias = (categorias:Categoria[])=>this.setState({categorias});
    setTalentosMasPopulares = (talentosMasPopulares:Talento[])=>this.setState({talentosMasPopulares});
    setTalentosBusqueda = (talentosBusqueda:Talento[])=>this.setState({talentosBusqueda});
    setTalentosList = (talentosList:Talento[])=>this.setState({talentosList});
    setTalentosSelected = (talentoSelected:Talento)=>this.setState({talentoSelected});
    setEstatus= (estatus:Estatus[])=>this.setState({estatus});
    logOut = () =>{

        const payload0 = this.state.sesion;
            payload0.token='';
            this.setState({payload0})
        

        const payload= this.state.usuario;
            payload.tipo=TipoUsuario.NONE;
            payload.email='';
            payload.password='';
            payload.nuevoPassword1='',
            payload.nuevoPassword2='',
            this.setState({payload})

        const payload2= this.state.flags;
            payload2.isLogedIn=false;
            //payload2.isAlertLoginVisible=false;

            this.setState({payload2})

        const payload3 = this.state.agenda;
            payload3.selectedDate ='';
            this.setState({payload3})
    
     }

     render(): React.ReactNode {
         return(
           <GeneralContext.Provider
             value={{
                    //properties
                    flags:this.state.flags,
                    ids:this.state.ids,
                    tabSelected: this.state.tabSelected,
                    tabSelectedOld: this.state.tabSelectedOld,
                    tabModule: this.state.tabModule,
                    categorias:this.state.categorias,
                    talentosMasPopulares:this.state.talentosMasPopulares,
                    talentosBusqueda:this.state.talentosBusqueda,
                    talentosList:this.state.talentosList,
                    talentoSelected:this.state.talentoSelected,
                    estatus:this.state.estatus,
                   //////////////////functions///////////////////////////
                    setFlags:this.setFlags,
                    setIds:this.setIds,
                    logOut: this.logOut,
                    setTabSelected: this.setTabSelected,
                    setTabSelectedOld: this.setTabSelectedOld,
                    setTabModule:this.setTabModule,
                    setCategorias:this.setCategorias,
                    setTalentosMasPopulares:this.setTalentosMasPopulares,
                    setTalentosBusqueda:this.setTalentosBusqueda,
                    setTalentosList:this.setTalentosList,
                    setTalentoSelected:this.setTalentosSelected,
                    setEstatus:this.setEstatus,
                    }}
                >
               {this.props.children}
           </GeneralContext.Provider>
         )
     }
}

export { GeneralProvider, GeneralContext }