import React, { useContext, useState } from 'react';
import { Modal, View, Text, TouchableOpacity, Platform } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { OpcionHeader } from '../../components/OpcionHeader';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ButtonSimple } from '../../components/home/ButtonSimple';
import { LabelTitulo } from '../../components/LabelTitulo';
import { Calendario } from '../../components/Calendario';
import { CarrouselHoras } from '../../components/home/CarrouselHoras';



export const ModalReservar = ( ) => {

   
      const { top } = useSafeAreaInsets();

      //call service to get data
      const { flags,setFlags,tabSelected,talentoSelected } = useContext(GeneralContext);
                                                                              
      return  <View >
                  <Modal animationType='fade' transparent={false}   visible={ flags.modalReservarVisible }>


                        <View style={{alignSelf:'flex-start',alignContent:'center',justifyContent:'center', 
                                          flexDirection:'row',top:top,backgroundColor:'white', height:110}}>
                                    
                                    {/* icono left side (hamburguer or back arrow) */}
                                    <OpcionHeader iconName='ic_baseline-arrow-back' color='black'  // back option 
                                          onPress={() =>{ 
                                                console.log('going back..')
                                                const payload= flags;
                                                payload.modalReservarVisible= false;
                                                payload.modalTalentoVisible= true;
                                                setFlags(payload);
                                          
                                          }} /> 
                        </View>


                        {/* disponibilidad */}
                        <View style={{marginHorizontal:-14,marginTop:10,marginBottom:3}}>
                              <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Disponibilidad'} separacion={10} alineacion='left'
                                          labelSubTitulo={'¡Aparta la fecha y asegura el mejor talento para tu evento¡'} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                        </View>

                        {/* selecciona una fecha */}
                        <View style={{marginHorizontal:-14,marginTop:25,marginBottom:-5}}>

                              <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Selecciona una fecha:'} separacion={10} alineacion='left'
                                          labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                                          {/* linea */}
                               <View style={{backgroundColor:'#838892', width:'100%',opacity:0.2,top:25, height:2, position:'absolute',}}></View>
                        </View>

                        <View style={{marginTop:-20,marginBottom:60}}>
                           <Calendario></Calendario>
                        </View>

                       
                        {/* selecciona una hora */}
                        <View style={{marginHorizontal:-14,marginTop:15,marginBottom:3,backgroundColor:'transparent'}}>
                              <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Selecciona una hora:'} separacion={10} alineacion='left'
                                          labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                               {/* linea */}
                        <View style={{flex:0, backgroundColor:'#838892', width:'100%',opacity:0.2,top:25, height:2, position:'absolute',}}></View>

                                <View style={{flex:0,marginLeft:30,marginRight:20, marginTop:-10}}>
                                    <CarrouselHoras></CarrouselHoras>
                                </View>
                        </View>
                       
                        {/* reservar event */}
                        <View style={{ flex:1,backgroundColor:'transparent',paddingTop:0,justifyContent:'center',alignContent:'center',alignItems:'center', }}>
                              <ButtonSimple disabled={(talentoSelected.diasSolicitados.length>0 && talentoSelected.horarioSeleccionado !='' ? false : true)}  color='white' label={'Reservar'} onPress={() => {
                              const payload = flags;
                              payload.modalReservarVisible = false;
                              payload.modalContratarVisible = true;
                              setFlags(payload);
                        } } backgroundColor={(talentoSelected.diasSolicitados.length>0 && talentoSelected.horarioSeleccionado !='' ? '#110331' : 'grey')} textDecorationLine={'none'}></ButtonSimple>
                        </View>

                  </Modal>
            </View>   
}
