import React, { useContext } from 'react';
import { Modal, View, Text, Image } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';

import { Spacer } from '../../components/Spacer';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ButtonSimple } from '../../components/home/ButtonSimple';
import { useNavigation } from '@react-navigation/native';
import { Step1 } from '../../components/guia/Step1';
import { gstyles } from '../../theme/appTheme';
import { Step2 } from '../../components/guia/Step2';
import { Step4 } from '../../components/guia/Step4';
import { Step3 } from '../../components/guia/Step3';


export const ModalGuia01 = ( ) => {

   
      const { top } = useSafeAreaInsets();
      const navigation = useNavigation();

      //call service to get data
      const { flags,setFlags } = useContext(GeneralContext);
      let stepRendereado;
      if(flags.guiaStep1){ stepRendereado =  <Step1></Step1>  }
      else if(flags.guiaStep2){ stepRendereado =  <Step2></Step2>  }
      else if(flags.guiaStep3){ stepRendereado =  <Step3></Step3>  }
      else if(flags.guiaStep4){ stepRendereado =  <Step4></Step4>  }
                                                                              
      return  <View>
                  <Modal animationType='fade' transparent={true}   visible={ flags.modalGuia01Visible } >
                    {stepRendereado}
                  </Modal>
            </View>   
}
