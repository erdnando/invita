import React, { useContext, useState } from 'react';
import { Modal, View, Text, TouchableOpacity, Platform, ScrollView } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useSearch } from '../../hooks/useSearch';
import { LabelTitulo } from '../../components/LabelTitulo';
import { EncabezadoTalento } from '../../components/pedido/EncabezadoTalento';
import { Select } from '../../components/Select';
import { OpcionesContratacion } from '../../components/OpcionesContratacion';
import { WithoutItems } from '../../components/search/WithoutItems';
import { CarrouselTalentoBusqueda } from '../../components/home/CarrouselTalentoBusqueda';
import { CarrouselResenias } from '../../components/home/CarrouselResenias';
import { ButtonSimple } from '../../components/home/ButtonSimple';
import { Talento } from '../../models/Talento';


export const ModalTalento = ( ) => {

   
      const { top } = useSafeAreaInsets();

      //call service to get data
      const { flags,setFlags,talentoSelected,ids,setIds,talentosBusqueda } = useContext(GeneralContext);
      const { onChangeSearch } = useSearch();
                                                                              
      return  <View >
                  <Modal animationType='fade' transparent={false}   visible={ flags.modalTalentoVisible }>

                  <View style={{flex:1}}>
                        {/* Encabezado */}
                        <EncabezadoTalento></EncabezadoTalento>

                              <View style={{top:-5,flex:1,backgroundColor:'white',borderRadius: 9, elevation:0,paddingHorizontal:5,width:'100%',
                              shadowColor: "black", shadowOpacity: 0.4,shadowOffset: { height: 1, width: 1 }}}>

                                    {/* Conoce al talento */}
                                    <View style={{marginHorizontal:-18,marginTop:20,marginBottom:3}}>
                                          <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Conoce al talento'} separacion={10} alineacion='left'
                                                      labelSubTitulo={talentoSelected.descripcion} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                                    </View>

                                   {/* opciones de contratacion */}
                                    <View style={{flex:0,height:120, marginHorizontal:-18,marginTop:14,}}>
                                          <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Opciones de contratacion'} separacion={0} alineacion='left'
                                                      labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                                          {/* integrantes & duration */}
                                          <OpcionesContratacion></OpcionesContratacion>
                                          <Text style={{marginLeft:30,textAlign:'left',fontStyle:'italic',fontSize:13}}>
                                                {'Costo estimado: $'+ validaCosto(talentoSelected) } 
                                          </Text>
                                    </View>

                                    {/* reseñas */} 
                                    <View style={{flex:1, marginHorizontal:-18,marginTop:10}}>
                                          <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Reseñas'} separacion={0} alineacion='left'
                                                      labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                                                <CarrouselResenias></CarrouselResenias> 
                                    </View>  

                                    {/* disponibilidad    */}
                                    <View style={{flex:1,position:'absolute', height:80,width:'105%',bottom:-5, backgroundColor:'#E9DEFB'}}>
                                        <View style={{flex:1,flexDirection:'row',}}>
                                                <LabelTitulo width={140} marginTop={13} fontWeight='400' fontWeight2='700' fontSizeTitulo={14} fontSizeSubtitulo={15} labelTitulo={'Si te gusta.'} separacion={0} alineacion='left'
                                                                  labelSubTitulo={'¡Reserva Ahora¡'} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>

                                                <ButtonSimple disabled={ parseFloat(talentoSelected.costoFinalmultiplicado)>0 ? false: true}  color='white' label={'Ver disponibilidad'} onPress={() => {
                                            
                                                      const payload= flags;
                                                      payload.modalTalentoVisible=false;
                                                      payload.modalReservarVisible= true;
                                                      setFlags(payload);

                                                } } backgroundColor={'#110331'} textDecorationLine={'none'}></ButtonSimple>
                                        </View>
                                    </View>

                              </View>
                  </View>
                  </Modal>
            </View>   
}
function validaCosto(talentoSelected: Talento) {
      return isNaN(parseFloat(talentoSelected.costoFinalmultiplicado)) ? "0.00" : parseFloat(talentoSelected.costoFinalmultiplicado).toFixed(2).toString();
}

