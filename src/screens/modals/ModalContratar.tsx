import React, { useContext, useState } from 'react';
import { Modal, View, Text } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { Spacer } from '../../components/Spacer';
import { OpcionHeader } from '../../components/OpcionHeader';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ButtonSimple } from '../../components/home/ButtonSimple';


export const ModalContratar = ( ) => {

   
      const { top } = useSafeAreaInsets();

      //call service to get data
      const { flags,setFlags,tabSelected,setTabSelected,setTabModule } = useContext(GeneralContext);
                                                                              
      return  <View >
                  <Modal animationType='fade' transparent={false}   visible={ flags.modalContratarVisible }>


                        <View style={{alignSelf:'flex-start',alignContent:'center',justifyContent:'center', 
                                          flexDirection:'row',top:top,backgroundColor:'white', height:110}}>
                                    
                                    {/* icono left side (hamburguer or back arrow) */}
                                    <OpcionHeader iconName='ic_baseline-arrow-back' color='black'  // back option 
                                          onPress={() =>{ 
                                                console.log('going back..')
                                                const payload= flags;
                                                payload.modalContratarVisible= false;
                                                payload.modalReservarVisible= true;
                                                setFlags(payload);
                                          
                                          }} /> 
                        </View>



                        <View style={{ flex:1,backgroundColor:'red',paddingTop:50,justifyContent:'center',alignContent:'center',alignItems:'center' }}>
                              <Text>contratacion de {tabSelected}</Text>

                            

                              <Spacer height={10}></Spacer>


                              <ButtonSimple   color='white' label={'finalizar...'} onPress={() => {
                              // console.log('reservar '+ {tabSelected})
                              // setTabSelected('artista '+ {tabSelected});
                              // setTabModule('artista '+ {tabSelected});
                              const payload = flags;
                              payload.modalContratarVisible = false;
                              // payload.modalArtistaVisible= false;
                              setFlags(payload);

                        } } backgroundColor={'red'} textDecorationLine={'none'}></ButtonSimple>

                        </View>

                  </Modal>
            </View>   
}
