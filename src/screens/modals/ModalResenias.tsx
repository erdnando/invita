import React, { useContext, useState } from 'react';
import { Modal, View, Text, TouchableOpacity, Platform } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { Spacer } from '../../components/Spacer';
import { OpcionHeader } from '../../components/OpcionHeader';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ButtonSimple } from '../../components/home/ButtonSimple';


export const ModalResenias = ( ) => {

   
      const { top } = useSafeAreaInsets();

      //call service to get data
      const { flags,setFlags,tabSelected,setTabSelected,setTabModule } = useContext(GeneralContext);
                                                                              
      return  <View >
                  <Modal animationType='fade' transparent={false}   visible={ flags.modalReseniaVisible }>


                        <View style={{alignSelf:'flex-start',alignContent:'center',justifyContent:'center', 
                                          flexDirection:'row',top:top,backgroundColor:'white', height:110}}>
                                    
                                    {/* icono left side (hamburguer or back arrow) */}
                                    <OpcionHeader iconName='ic_baseline-arrow-back' color='black'  // back option 
                                          onPress={() =>{ 
                                                console.log('going back..xx')
                                                const payload= flags;
                                                payload.modalReseniaVisible= false;
                                                payload.modalTalentoVisible=true;
                                                setFlags(payload);
                                          
                                          }} /> 
                        </View>



                        <View style={{ flex:1,backgroundColor:'white',paddingTop:50,justifyContent:'center',alignContent:'center',alignItems:'center' }}>
                              <Text>reseñas del Artista</Text>

                            

                              <Spacer height={10}></Spacer>


                            

                        </View>

                  </Modal>
            </View>   
}
