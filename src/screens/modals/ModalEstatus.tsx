import React, { useContext, useState } from 'react';
import { Modal, View, Text, TouchableOpacity, Platform } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { colores, gstyles } from '../../theme/appTheme';
import CustomIcon from '../../theme/CustomIcon';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useAgenda } from '../../hooks/useAgenda';
import { Spacer } from '../../components/Spacer';
import { OpcionHeader } from '../../components/OpcionHeader';
import { useSafeAreaInsets } from 'react-native-safe-area-context';


export const ModalEstatus = ( ) => {

   
      const { top } = useSafeAreaInsets();

      //call service to get data
      const { flags,setFlags,setAgendaFiltro,agendaFiltro } = useContext(GeneralContext);
                                                                              
      return  <View >
                  <Modal animationType='fade' transparent={false}   visible={ flags.resultadosBusquedaVisible }>


                        <View style={{alignSelf:'flex-start',alignContent:'center',justifyContent:'center', 
                                          flexDirection:'row',top:top,backgroundColor:'white', height:110}}>
                                    
                                    {/* icono left side (hamburguer or back arrow) */}
                                    <OpcionHeader iconName='ic_baseline-arrow-back' color='black'  // back option 
                                          onPress={() =>{ 
                                          console.log('going back..')
                                          const payload= flags;
                                          payload.modalEstatusVisible= false;
                                          setFlags(payload);
                                          
                                          }} /> 
                        </View>



                        <View style={{ flex:1,backgroundColor:'white',paddingTop:50,justifyContent:'center',alignContent:'center',alignItems:'center' }}>
                              <Text>Estatus del pedido</Text>

                        </View>

                  </Modal>
            </View>   
}
