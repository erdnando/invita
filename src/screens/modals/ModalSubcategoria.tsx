import React, { useContext, useState } from 'react';
import { Modal, View, Text, TouchableOpacity, Platform } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { colores, gstyles } from '../../theme/appTheme';
import CustomIcon from '../../theme/CustomIcon';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useAgenda } from '../../hooks/useAgenda';
import { Spacer } from '../../components/Spacer';
import { OpcionHeader } from '../../components/OpcionHeader';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ButtonSimple } from '../../components/home/ButtonSimple';


export const ModalSubcategoria = ( ) => {

   
      const { top } = useSafeAreaInsets();

      //call service to get data
      const { flags,setFlags,tabSelected,setTabSelected,setTabModule } = useContext(GeneralContext);
                                                                              
      return  <View >
                  <Modal animationType='fade' transparent={false}   visible={ flags.resultadosBusquedaVisible }>


                        <View style={{alignSelf:'flex-start',alignContent:'center',justifyContent:'center', 
                                          flexDirection:'row',top:top,backgroundColor:'white', height:110}}>
                                    
                                    {/* icono left side (hamburguer or back arrow) */}
                                    <OpcionHeader iconName='ic_baseline-arrow-back' color='black'  // back option 
                                          onPress={() =>{ 
                                                console.log('going back..')
                                                const payload= flags;
                                                payload.modalSubCategoriaVisible= false;
                                                setFlags(payload);
                                          
                                          }} /> 
                        </View>



                        <View style={{ flex:1,backgroundColor:'white',paddingTop:50,justifyContent:'center',alignContent:'center',alignItems:'center' }}>
                              <Text>Subcategorias de: {tabSelected}</Text>



                              <Spacer height={50}></Spacer>
                                {/* <ButtonSimple  iconClose='ic_round-close' color='white' label={'Buscar...'} onPress={ ()=> {
                                        console.log('Buscando...')
                                        setTabSelected('Buscando');
                                        setTabModule('Buscando');

                                        const payload= flags;
                                        payload.modalSubCategoriaVisible= true;
                                        setFlags(payload);
                                    } }></ButtonSimple>
                                <Spacer height={10}></Spacer> */}

                               


                                <ButtonSimple  iconClose='ic_round-close' color='white' label={'Ver artista...AAA'} onPress={ ()=> {
                                        console.log('ver Artista...')
                                        setTabSelected('Artista AAA');
                                        setTabModule('Artista AAA');
                                  
                                        const payload= flags;
                                        payload.modalSubCategoriaVisible=false;
                                        payload.modalArtistaVisible= true;
                                        setFlags(payload);

                                    } }></ButtonSimple>

                                <Spacer height={10}></Spacer>

                                <ButtonSimple  iconClose='ic_round-close' color='white' label={'Ver artista...BBB'} onPress={ ()=> {
                                        console.log('ver Artista...')
                                        setTabSelected('Artista BBB');
                                        setTabModule('Artista BBB');
                                  
                                        const payload= flags;
                                        payload.modalSubCategoriaVisible=false;
                                        payload.modalArtistaVisible= true;
                                        setFlags(payload);

                                    } }></ButtonSimple>



                                <Spacer height={10}></Spacer>

                        </View>

                  </Modal>
            </View>   
}
