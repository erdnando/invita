import { useNavigation } from '@react-navigation/native';
import React, { useContext, useEffect } from 'react'
import { Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View, Platform,KeyboardAvoidingView,ScrollView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import { ScrollView } from 'react-native-gesture-handler';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Toast from 'react-native-toast-message';
import KeyboardWrapper from '../../components/KeyboardWrapper';
import { Loading } from '../../components/Loading';
import { ButtonRounded } from '../../components/login/ButtonRounded';
import { ButtonTextGoTo } from '../../components/login/ButtonTextGoTo';
import { InputEmail } from '../../components/login/InputEmail';
import { Spacer } from '../../components/Spacer';
import { useLogin } from '../../hooks/useLogin';
import { GeneralContext } from '../../state/GeneralProvider';
import CustomIcon from '../../theme/CustomIcon';
// import KeyboardWrapper from '../../components/KeyboardWrapper';

export const ForgotPasswordScreen = () => {

    const { top,left } = useSafeAreaInsets();
    const navigation = useNavigation();
    const { setPasswordAux,setEmailAux,validarReset } = useLogin(); 
    const { flags } = useContext( GeneralContext )
    const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0

    useEffect(() => {
        setPasswordAux('.');
    }, [])

    useEffect(() => {
        navigation.setOptions(
            {
                headerLeft: () => {
                    return   <TouchableOpacity  
                               onPress={() =>{ setPasswordAux('');setEmailAux(''); navigation.goBack();   }}>
                                <Text style={{left:18}}>
                                <CustomIcon name='ic_baseline-arrow-back' size={30} color='black' ></CustomIcon>
                                </Text>
                            </TouchableOpacity>
                       
                   },
                title:'Reset de Senha',
            }
            )
    }, [])

    if(flags.isLoadingforgotPassword){
        return <Loading loadingSize={40} color='orange'></Loading>
      }
    
    return (
        <KeyboardAwareScrollView style={{ width:'100%', height:'100%', flex:1, }} bounces={false} contentContainerStyle={{flexGrow: 1}}>
               <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={{...styles.container , marginHorizontal:left-25}}>
             <ImageBackground style={styles.background} resizeMode='cover' source={require('../../assets/Background.png')}>
             
                    <View style={{flex:1,flexDirection:'column', justifyContent:'space-between',backgroundColor:'transparent'}}>
                        <Spacer height={40} ></Spacer>
                        <View style={styles.logo} ><Image source={require('../../assets/vertical-logo.png')} ></Image></View>
                        <Spacer height={50} ></Spacer>
    
                        
                        <View style={styles.formulario}>
                            <View style={{ alignItems:'center'}}>
                                <Text style={{fontSize:25,fontFamily:'Roboto-Regular', fontWeight:'bold',color:'#757575'}}>Esqueceu sua senha?</Text>
                                <Spacer height={20} ></Spacer>
                                <Text style={{fontSize:17, fontFamily:'Roboto-Regular',color:'#757575'}}>digite seu e-mail abaixo para receber</Text>
                                <Text style={{fontSize:17,fontFamily:'Roboto-Regular',color:'#757575'}}>instruções de como resetar sua senha</Text>
                        </View>

                        <Spacer height={20} ></Spacer>
                        <InputEmail placeHolderTextColor='#000000' modo='normal' label='E-mail' iconLeft='ic_outline-email' iconRight='ic_round-close'></InputEmail>

                        <Spacer height={40} ></Spacer>

                        <View>
                            <ButtonRounded label='SOLICITAR' 
                                        onPress={ async() =>  { 
                                    
                                            let resp = await validarReset();
                                            
                                            if(resp){
                                                Toast.show({type: 'ok',props: { mensaje: 'Solicitação enviada' }});
                                                console.log('solicitud enviada')
                                                navigation.navigate({name:'ResetContrasenaScreen'} as never); 
                                            }   

                                        }} />
                            </View>
                        </View>
                        <Spacer height={50} ></Spacer>
                    <ButtonTextGoTo label='Voltar para Login' bottom={30} onPress={ async() =>  { 
                                setPasswordAux('');
                                setEmailAux('');
                                navigation.navigate({name:'LoginScreen'} as never); 
                                }} ></ButtonTextGoTo>
                    </View>
            </ImageBackground>
        </View>

        </ScrollView>
        </KeyboardAwareScrollView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'flex-end',
 
    },
    background:{
        flex:1, justifyContent:'center',
    },
    logo:{
        justifyContent:'center', alignItems:'center'
    },
    formulario:{
        flex:1,
       
    },
    
})