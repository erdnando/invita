import React, { useContext, useEffect } from 'react'
import {  StyleSheet, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useMovies } from '../../hooks/useMovies';
import { GeneralContext } from '../../state/GeneralProvider';
import { Loading } from '../../components/Loading';
import { ScrollView } from 'react-native-gesture-handler';
import { ModalEstatus } from '../modals/ModalEstatus';
import { ModalBuscador } from '../modals/ModalBuscador';
import { useNavigation } from '@react-navigation/native';
import { ModalSubcategoria } from '../modals/ModalSubcategoria';
import { ModalReservar } from '../modals/ModalReservar';
import { ModalContratar } from '../modals/ModalContratar';
import { ModalGuia01 } from '../modals/ModalGuia01';
import { LabelTitulo } from '../../components/LabelTitulo';
import { Encabezado } from '../../components/home/Encabezado';
import { Carrousel } from '../../components/home/Carrousel';
import { CarrouselTalento } from '../../components/home/CarrouselTalento';
import { InputSearchEvento } from '../../components/InputSearchEvento';
import { useSearch } from '../../hooks/useSearch';




export const HomeScreen = () => {
    const { top } = useSafeAreaInsets();
    const { flags } = useContext(GeneralContext);
    const { isLoading } = useMovies();
    const navigation = useNavigation()
    const { getPopulares } = useSearch();

    useEffect(() => {
        getPopulares();
    }, []);
  
    //https://github.com/osdnk/react-native-reanimated-bottom-sheet/issues/243#issuecomment-644091552
    //https://github.com/osdnk/react-native-reanimated-bottom-sheet
  
       if(flags.modalGuia01Visible==true){
        return   <ModalGuia01></ModalGuia01>
    }
     if(flags.modalEstatusVisible){
        return   <ModalEstatus ></ModalEstatus>           
    }
      if(flags.modalBuscadorVisible){
        return   <ModalBuscador ></ModalBuscador>
    }

     if(isLoading){
        return <Loading loadingSize={40} color='orange'></Loading>
    }

    
   
    

    {
       return ( 
                <View style={{flex:1}}>
                    {/* Encabezado */}
                    <Encabezado searchTitulo='¿Que desea buscar..'></Encabezado>


                    <ScrollView style={{top:-5,flex:1,backgroundColor:'white',borderRadius: 9, elevation:0,paddingHorizontal:5,width:'100%',
                            shadowColor: "black", shadowOpacity: 0.4,shadowOffset: {
                            height: 1,
                            width: 1
                            }}}>


                            <View style={{marginHorizontal:-18,marginTop:14}}>

                                <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Categorias'} separacion={0} alineacion='left'
                                             labelSubTitulo={'Descubre nuestras categorias'} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                                {/* categorias horizontal */}
                                <View style={{flex:1,marginLeft:30,marginRight:30, marginTop:14}}>
                                    <Carrousel></Carrousel>
                                </View>

                            </View>


                            <View style={{marginHorizontal:-18,marginTop:14}}>
                                <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Mas populares'} separacion={0} alineacion='left'
                                             labelSubTitulo={'Los favoritos de Mexico'} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                                {/* mas populares carrousel horizontal */}
                                <View style={{flex:1,marginLeft:30,marginRight:30, marginTop:14}}>
                                    <CarrouselTalento></CarrouselTalento>
                                </View>
                            
                            </View>

                           

                            <View style={{marginHorizontal:-18,marginTop:14}}>
                                <LabelTitulo fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Mi Evento'} separacion={0} alineacion='left'
                                             labelSubTitulo={'Verifica el estatus de tu talento'} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                                {/* mas populares carrousel horizontal */}
                                <View style={{flex:1,marginLeft:14,marginRight:10, marginTop:4}}>
                                    <InputSearchEvento></InputSearchEvento>
                                </View>
                            
                            </View>           
                        </ScrollView>
                </View>
            )
        }
}


const styles = StyleSheet.create({
    background:{
        flex:1, justifyContent:'center',
    },
});