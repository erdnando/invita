import React, { useContext } from 'react'
import { ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { GeneralContext } from '../../state/GeneralProvider';
import { Loading } from '../../components/Loading';
import { ListNotificaciones } from '../../components/notificaciones/ListNotificaciones';
import { Spacer } from '../../components/Spacer';
import { ButtonSimple } from '../../components/home/ButtonSimple';
import { useNavigation } from '@react-navigation/native';
import { ModalEstatus } from '../modals/ModalEstatus';
import { ModalBuscador } from '../modals/ModalBuscador';
import { ModalSubcategoria } from '../modals/ModalSubcategoria';
import { CarrouselCategoriasVertical } from '../../components/home/CarrouselCategoriasVertical';
import { LabelTitulo } from '../../components/LabelTitulo';
import { InputSearchEventoFiltro } from '../../components/InputSearchEventoFiltro';
import { LabelBusqueda } from '../../components/LabelBusqueda';
import { InputSearchEventoBuscar } from '../../components/InputSearchEventoBuscar';

export const CategoriaScreen = () => {

    const { top } = useSafeAreaInsets();
    //call global state
    const { flags,setFlags,setTabSelected,setTabModule} = useContext(GeneralContext);
    const navigation = useNavigation();


  
//  if(flags.modalBuscadorVisible){
//      return   <ModalBuscador ></ModalBuscador>
         
//   }

//   if(flags.modalSubCategoriaVisible){
//      return   <ModalSubcategoria ></ModalSubcategoria>
         
//   }

//   if(flags.modalArtistaVisible){
//     return   <ModalArtista ></ModalArtista>
        
//  }


   
        return(
            <View style={ {  flex:1,}}>

              <ImageBackground style={styles.background}  resizeMode='cover' source={require('../../assets/Background.png')}>

                    <View style={{flex:1,marginTop:60}}>
                        <LabelTitulo marginHorizontal={18} fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Categorias'} separacion={0} alineacion='left'
                                                    labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                        <InputSearchEventoBuscar label={'¿Que deas buscar?' } iconLeft={'ic_baseline-search'} iconRight={'ic_baseline-arrow-forward'} color={'#110331'} marginTop={-15} ></InputSearchEventoBuscar>
                        
                       
                    

                        <View style={{flex:1,marginHorizontal:18, marginTop:30}}>
                           
                           <CarrouselCategoriasVertical></CarrouselCategoriasVertical>
                        </View>
                    </View>

              </ImageBackground>
        </View>
            
        )
}

const styles = StyleSheet.create({
    background:{
        flex:1, justifyContent:'center',
    },
});