import React, { useContext } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { GeneralContext } from '../../state/GeneralProvider';
import { Encabezado } from '../../components/pedido/Encabezado';
import { LabelSeccion } from '../../components/LabelSeccion';
import { LabelResumen } from '../../components/LabelResumen';
import { ListEstatus } from '../../components/pedido/ListEstatus';

export const PedidoScreen = () => {
    const { top } = useSafeAreaInsets();
    const { flags,ids } = useContext(GeneralContext);

  
    //https://github.com/osdnk/react-native-reanimated-bottom-sheet/issues/243#issuecomment-644091552
    //https://github.com/osdnk/react-native-reanimated-bottom-sheet
  
    
    //  if(isLoading){
    //     return <Loading loadingSize={40} color='orange'></Loading>
    // }
    

    {
       return ( 
                <View style={{flex:1}}>
                    {/* Encabezado */}
                    <Encabezado searchTitulo='Ingresa tu codigo'></Encabezado>


                    {/* <ScrollView style={{top:-5,flex:1,backgroundColor:'white',borderRadius: 9, elevation:0,paddingHorizontal:5,width:'100%',
                            shadowColor: "black", shadowOpacity: 0.4,shadowOffset: {
                            height: 1,
                            width: 1
                            }}}> */}
                            <View style={{top:-5,flex:1,backgroundColor:'white',borderRadius: 9, elevation:0,paddingHorizontal:5,width:'100%',
                            shadowColor: "black", shadowOpacity: 0.4,shadowOffset: {
                            height: 1,
                            width: 1
                            }}}>


                            <View style={{flex:0, marginHorizontal:-18,marginTop:14}}>

                        

                               {/* Row resumen, folio */}
                                <View style={{ marginTop:4,flexDirection:'row',height:40}}>
                                  <LabelResumen fontSizeTitulo={18} separacion={2} fontSizeSubtitulo={1} labelTitulo={'Resumen'} alineacion={'left'} labelSubTitulo={''} colorTitulo={'black'} colorSubTitulo={'black'}></LabelResumen>
                                  <LabelResumen fontSizeTitulo={13} separacion={2} fontSizeSubtitulo={1} labelTitulo={ ids.codigoEvento } tituloMarginTop={5} alineacion={'left'} labelSubTitulo={''} colorTitulo={'red'} colorSubTitulo={'black'}></LabelResumen>
                                </View>
                                
                                {/* Row nombre, hora */}
                                <View style={{marginTop:5,flexDirection:'row'}}>
                                  <LabelResumen fontSizeTitulo={13} separacion={2} fontSizeSubtitulo={12} labelTitulo={'Nombre del cliente'} alineacion={'left'} labelSubTitulo={'Daniela cova'} colorTitulo={'black'} colorSubTitulo={'black'}></LabelResumen>
                                  <LabelResumen fontSizeTitulo={13} separacion={2} fontSizeSubtitulo={12} labelTitulo={'Hora evento'} alineacion={'left'} labelSubTitulo={'22:00PM'} colorTitulo={'black'} colorSubTitulo={'black'}></LabelResumen>
                                </View>

                                {/* Row talento,integrantes */}
                                <View style={{marginTop:10,flexDirection:'row'}}>
                                  <LabelResumen fontSizeTitulo={13} separacion={2} fontSizeSubtitulo={12} labelTitulo={'Talento'} alineacion={'left'} labelSubTitulo={'Banda Mariachis el Sol'} colorTitulo={'black'} colorSubTitulo={'black'}></LabelResumen>
                                  <LabelResumen fontSizeTitulo={13} separacion={2} fontSizeSubtitulo={12} labelTitulo={'Integrantes'} alineacion={'left'} labelSubTitulo={'5'} colorTitulo={'black'} colorSubTitulo={'black'}></LabelResumen>
                                </View>

                                {/* Row fecha,costo */}
                                <View style={{ marginTop:10,flexDirection:'row'}}>
                                  <LabelResumen fontSizeTitulo={13} separacion={2} fontSizeSubtitulo={12} labelTitulo={'Fecha contratacion'} alineacion={'left'} labelSubTitulo={'12/07/2022'} colorTitulo={'black'} colorSubTitulo={'black'}></LabelResumen>
                                  <LabelResumen fontSizeTitulo={13} separacion={2} fontSizeSubtitulo={12} labelTitulo={'Costo total'} alineacion={'left'} labelSubTitulo={'$6,500.00'} colorTitulo={'black'} colorSubTitulo={'black'}></LabelResumen>
                                </View>

                                
                            </View>




                            <View style={{marginHorizontal:10,marginTop:20,justifyContent:'flex-start',alignItems:'flex-start',alignContent:'flex-start'}}>
                               
                               <LabelSeccion fontSizeTitulo={18} labelTitulo={'Estatus de tu talento'}  alineacion='left' colorTitulo={'#110331'} ></LabelSeccion>
                               <ListEstatus></ListEstatus>
                            </View>
      
                        {/* </ScrollView> */}
                        </View>
                  
                  
                </View>
            )
        }
   
}


const styles = StyleSheet.create({
    background:{
        flex:1, justifyContent:'center',
    },
});