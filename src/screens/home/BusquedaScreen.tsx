import React, { useContext } from 'react'
import { ImageBackground, KeyboardAvoidingView, Modal, ScrollView, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { InputSearchEventoFiltro } from '../../components/InputSearchEventoFiltro';
import { LabelTitulo } from '../../components/LabelTitulo';
import { Loading } from '../../components/Loading';
import { Spacer } from '../../components/Spacer';
import { GeneralContext } from '../../state/GeneralProvider';
import { LabelBusqueda } from '../../components/LabelBusqueda';
import { CarrouselTalentoBusqueda } from '../../components/home/CarrouselTalentoBusqueda';
import { WithoutItems } from '../../components/search/WithoutItems';
import CustomIcon from '../../theme/CustomIcon';
import { LabelFiltroLimpiar } from '../../components/LabelFiltroLimpiar';
import { CheckBoxSeccion } from '../../components/search/CheckBoxSeccion';
import { TouchableButton } from '../../components/search/TouchableButton';
import { useSearch } from '../../hooks/useSearch';
import { ModalTalento } from '../modals/ModalTalento';
import { ModalResenias } from '../modals/ModalResenias';
import { ModalReservar } from '../modals/ModalReservar';
import { ModalContratar } from '../modals/ModalContratar';


export const BusquedaScreen = () => {
    const { top } = useSafeAreaInsets();
    //call global state
    const { flags,ids,talentosBusqueda,setFlags } = useContext(GeneralContext);
    const { applyOrder,onChangeSearch } = useSearch();
    const defaultValor=false;
    const defaultValorTrue=true;
  
    if(flags.modalTalentoVisible){
        return   <ModalTalento ></ModalTalento>           
    }

    if(flags.modalReservarVisible){
        return   <ModalReservar ></ModalReservar>
    }

      if(flags.modalReseniaVisible){
        return   <ModalResenias ></ModalResenias>
    }

    if(flags.modalContratarVisible){
        return   <ModalContratar ></ModalContratar>
    }
    
    return (
        <View style={ {  flex:1,}}>
            <ImageBackground style={styles.background}  resizeMode='cover' source={require('../../assets/Background.png')}>

            <View style={{flex:1,marginTop:60}}>
                  <LabelTitulo marginHorizontal={18} fontSizeTitulo={18} fontSizeSubtitulo={14} labelTitulo={'Talentos'} separacion={0} alineacion='left'
                                             labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                  <InputSearchEventoFiltro label={'¿Que deas buscar?' } iconLeft={'ic_baseline-search'} iconRight={'ic_baseline-filter-list'} color={'#110331'} marginTop={-15} ></InputSearchEventoFiltro>
                  
                  <LabelBusqueda marginHorizontal={18} fontSize={14} label1={'Se han encontrado'} 
                    color={'#110331'} label2={talentosBusqueda.length.toString()} label3={'resultados con'} label4={'"'+ ids.busquedaTalento +'"'}  ></LabelBusqueda>

                  <View style={{flex:1,marginHorizontal:18, marginTop:30}}>
                     {talentosBusqueda.length >0 ? 
                     <CarrouselTalentoBusqueda></CarrouselTalentoBusqueda> :
                      <WithoutItems label={'xxx'}></WithoutItems> }
                  </View>
            </View>

            </ImageBackground>





        <Modal  animationType="slide"  transparent={true} visible={flags.modalFiltrosVisible}>

            <View style={{flex:1 , backgroundColor: 'rgba(0,0,0,0.3)',  }}>
                <View style={{flex:0,height:'75%',marginTop: 'auto', backgroundColor:'#FFFFFF',flexDirection:'column',
                            borderTopRightRadius:9,borderTopLeftRadius:9,
                            shadowColor: "black", shadowOpacity: 0.7,shadowOffset: { height: 1, width: 1 }  }}>

                            {/* close X */}
                            <View style={{flex:0,height:42,width:'100%',justifyContent:'flex-start',alignContent:'flex-end',alignItems:'flex-end'}}>
                                <TouchableOpacity activeOpacity={0.2}  style={{backgroundColor:'transparent',height:42,width:60, flex:0,shadowColor: "black",
                                alignItems:'center', borderRadius:9, shadowOpacity: 0.4,shadowOffset: {
                                                        height: 1, width: 1 },}} onPress={() =>{ 
                                            
                                                
                                                        //
                                                        const payload= flags;
                                                        payload.modalFiltrosVisible= false;
                                                        setFlags(payload);

                                                        onChangeSearch('')
                                }}>
                                    
                                <View style={{flex:1,height:40,marginTop:10, marginHorizontal:10,alignContent:'flex-end',alignItems:'flex-end', backgroundColor:'transparent', }}>
                                        <CustomIcon   name={'ic_baseline-close'} size={24} color= {'black'}  ></CustomIcon>
                                </View>
                                    
                                </TouchableOpacity>
                            </View>

                            {/* titulo filtros */}
                            <LabelFiltroLimpiar fontSizeTitulo={17} fontSizeSubtitulo={16} labelTitulo={'Filtros para tu busqueda'} labelSubTitulo={'Limpiar'} colorTitulo={'black'} colorSubTitulo={'grey'}></LabelFiltroLimpiar>

                            <Spacer height={15}></Spacer>
                            <LabelTitulo marginHorizontal={15} fontSizeTitulo={15} fontSizeSubtitulo={12} labelTitulo={'Por valoracion'} separacion={0} alineacion='left'
                                             labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                           
                            <CheckBoxSeccion></CheckBoxSeccion>

                            <Spacer height={10}></Spacer>
                            <LabelTitulo marginHorizontal={15} fontSizeTitulo={15} fontSizeSubtitulo={12} labelTitulo={'Ordenar por'} separacion={0} alineacion='left'
                                             labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                            

                            {/* touchable button row1 */}
                            <View style={{flex:0,flexDirection:'row',width:'100%',height:40,justifyContent:'space-between', alignItems:'center' }}>
                                <TouchableButton disabled={false} backgroundColor={flags.orderByMasPoulares ? '#E42381':'white'} color={flags.orderByMasPoulares ? 'white':'grey'} tittle='Mas popular'
                                    onPress={function (): void {

                                        applyOrder(defaultValorTrue,defaultValor,defaultValor,defaultValor)
                                
                                    } }></TouchableButton>

                                <TouchableButton disabled={false} backgroundColor={flags.orderByMasRecientes ? '#E42381':'white'}  color={flags.orderByMasRecientes ? 'white':'grey'} tittle='Mas recientes'
                                    onPress={function (): void {
                                        applyOrder(defaultValor,defaultValor,defaultValorTrue,defaultValor)
                                    } }></TouchableButton>
                            </View>



                            {/* touchable button row2 */}
                            <View style={{flex:0,flexDirection:'row',width:'100%',height:40,justifyContent:'space-between', alignItems:'center',marginTop:12 }}>
                                <TouchableButton disabled={false} backgroundColor={flags.orderByMenorMayorPrecio ? '#E42381':'white'} color={flags.orderByMenorMayorPrecio ? 'white':'grey'} tittle='Menor a mayor precio'
                                    onPress={function (): void {
                                        applyOrder(defaultValor,defaultValorTrue,defaultValor,defaultValor)
                                    } }></TouchableButton>

                                <TouchableButton disabled={false} backgroundColor={flags.orderByMayorMenorPrecio ? '#E42381':'white'}  color={flags.orderByMayorMenorPrecio ? 'white':'grey'} tittle='Mayor a menor precio'
                                    onPress={function (): void {
                                        applyOrder(defaultValor,defaultValor,defaultValor,defaultValorTrue)
                                    } }></TouchableButton>
                            </View>

                           {/* apply button */}
                            <View style={{flex:1,width:'100%',backgroundColor:'transparent', justifyContent:'center',alignItems:'center',alignContent:'center'}}>
                                <TouchableOpacity activeOpacity={0.2}   disabled={false}  style={{backgroundColor:'#110331',height:45,width:140,flex:0,marginTop:-20, shadowColor: "black",
                                    borderRadius:9, marginHorizontal:18, shadowOpacity: 0.4,shadowOffset: {
                                    height: 1, width: 1 },}} onPress={function (): void {
                                        const payload= flags;
                                        payload.modalFiltrosVisible= false;
                                        setFlags(payload);

                                        onChangeSearch('')
                                    } }>

                                <View style={{flex:1, alignContent:'center',alignItems:'center',justifyContent:'center',  borderRadius:9,
                                borderColor:'black' }}>
                                <Text style={{ fontWeight:'600',color:'white',fontSize:12}}>{'Aplicar'}</Text>
                                </View>

                                </TouchableOpacity>
                            </View>
                             
                            
                          


                </View>
            </View>
        </Modal>



        </View>
    
    )
}



const styles = StyleSheet.create({
    background:{
        flex:1, justifyContent:'center',
    },
});