
import { createStackNavigator } from '@react-navigation/stack';
import React, { useContext, useEffect } from 'react'
import { ForgotPasswordScreen } from '../screens/login/ForgotPasswordScreen';
import { LoginScreen } from '../screens/login/LoginScreen';
import { ResetContrasenaScreen } from '../screens/login/ResetContrasenaScreen';
import { NavigationHome } from './NavigationHome';
import { NavigationLateral } from './NavigationLateral';
import SplashScreen from 'react-native-splash-screen'
import { ModalGuia01 } from '../screens/modals/ModalGuia01';
import { GeneralContext } from '../state/GeneralProvider';


const Stack = createStackNavigator();

export const NavigationLogin = () => {
  const { flags } = useContext(GeneralContext);
  
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  

 

  return (
    <Stack.Navigator   
    screenOptions={
        {
          cardStyle:{
            backgroundColor:'white',
          },
          headerShown:false,
          headerStyle:{
            elevation:0,
            shadowColor:'transparent' 
          },
          headerTitleStyle:{
            fontFamily:'Roboto-Regular'
          }
          
        }
      }
    >
      {/* <Stack.Screen name="LoginScreen" component={ LoginScreen } />
      <Stack.Screen name="ForgotPasswordScreen" component={ ForgotPasswordScreen } /> */}
       <Stack.Screen name="NavigationLateral" component={ NavigationLateral } />
      <Stack.Screen name="NavigationHome" component={ NavigationHome } />
     
      {/* <Stack.Screen name="ResetContrasenaScreen" component={ ResetContrasenaScreen } />
       */}
    </Stack.Navigator>
  
  );
}