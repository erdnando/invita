import React, { useContext } from 'react';  
import { Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { colores, gstyles } from '../theme/appTheme';
import { CategoriaScreen } from '../screens/home/CategoriaScreen';
import { PedidoScreen } from '../screens/home/PedidoScreen';
import { HomeScreen } from '../screens/home/HomeScreen';
import { DrawerScreenProps } from '@react-navigation/drawer';
import { OpcionBottomTab } from '../components/login/OpcionBottomTab';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { GeneralContext } from '../state/GeneralProvider';
import { BusquedaScreen } from '../screens/home/BusquedaScreen';

const Tab = createBottomTabNavigator();
interface Props extends DrawerScreenProps<any, any>{};


export const NavigationHome = ( { navigation }:Props) => {

  const { top } = useSafeAreaInsets();
  //call global state
  const { flags,setTabSelected,ids, setTabModule,} = useContext(GeneralContext);



      return(
        <Tab.Navigator  sceneContainerStyle={{ backgroundColor:'transparent',  }}  
        screenOptions={({route}) => ({
                tabBarActiveTintColor:colores.primary,
                tabBarInactiveTintColor:'white',
                tabBarStyle:{
                    backgroundColor: colores.bottomBar,
                    borderTopColor: colores.bottomBar,
                    borderTopWidth:1,
                    borderBottomWidth:1,
                    elevation:0,
                    height:Platform.OS === 'ios' ? 72 : 60
                },
                tabBarLabelStyle:{
                fontSize:13,
                },
                headerShown:true,
                
                tabBarIcon: ({color, focused, size }) =>{
                    return <OpcionBottomTab routeName={route.name} color={color} ></OpcionBottomTab>
                },
                header: ()=>{
                  return <View></View>
                }
                // header://header config
                //   () => {
                //     return <View style={{alignSelf:'flex-start',alignContent:'center',justifyContent:'center', 
                //                         flexDirection:'row',top:top,backgroundColor:colores.topBar, height:66}}>
                                
                //                {/* icono left side (hamburguer or back arrow) */}
                //                {  ids.idOpinionSeleccionado !='' ?   
                //                   <OpcionHeader iconName='ic_baseline-arrow-back' color={colores.primary}  // back option 
                //                       onPress={() =>{ 
                //                         console.log('going back..')
                //                        // goListParecer();
                                     
                //                       }} /> : 
                                    
                //                     <OpcionHeader iconName='ic_baseline-menu' color={colores.primary}  //menu hamburguesa
                //                       onPress={() =>{ 
                //                         navigation.toggleDrawer(); 
                //                       }} />
                //                }
                                  

                //                 {/* logo app/ menu option */}
                //                 <View style={{flex:1, left:24,top:15 }}>
                //                    <TitleApp></TitleApp>
                //                 </View>
                              
                //                {/* iconos right side */}
                //                 <View style={{ flexDirection:'row',alignSelf:'flex-end', top:-20   }} >  
                //                       {/* notificaciones */}
                //                       <TouchableOpacity 
                //                       onPress={() =>{  
                                          
                                            
                //                       }}>
                //                         {/* dot notification */}
                //                         <View style={{flex:1, flexDirection:'row',height:30}}>
                //                             <Image style={{...gstyles.avatar,height:28,width:25, top:3,tintColor:'white'}} 
                //                                 source={require('../assets/clarity_notification-solid-badged.png')}  >
                //                             </Image>
                //                             { flags.existsNotification ? <Text style={{ fontSize:75,color:  'orange',marginTop:Platform.OS=='ios' ? -61 : -78,right:11}}>.</Text>
                //                             : <Text style={{ fontSize:75,color:'black',marginTop:Platform.OS=='ios' ? -61 : -77,  right:12}}>.</Text>}
                //                         </View>

                //                       </TouchableOpacity>

                //                       {/* perfil */}
                //                       <TouchableOpacity style={{ marginRight:10, marginLeft:16, marginEnd:16 }} 
                //                       onPress={() =>{  navigation.toggleDrawer(); }}>
                //                         <Text>
                //                             <CustomIcon name='gridicons_user' size={30} color='white' style={{padding:150}} ></CustomIcon>
                //                         </Text>
                //                       </TouchableOpacity>

                                      

                //                 </View>
                //           </View>
                //   }
        })} >

        <Tab.Screen name="HomeScreen" options={{ title:'',unmountOnBlur:true }}   component={ HomeScreen } listeners={({ navigation, route }) => ({
                    tabPress: e => {  
                      setTabSelected('Logo');   
                      setTabModule('Logo');


                       }, })} />
         <Tab.Screen  name="BusquedaScreen" options={{ title:'',unmountOnBlur:true }} component={ BusquedaScreen } listeners={({ navigation, route }) => ({
                    tabPress: e => { 
                      setTabSelected('Busqueda'); 
                      setTabModule('Busqueda'); 
                  

                    }, })} />
        <Tab.Screen name="CategoriaScreen" options={{ title:'',unmountOnBlur:true }} component={ CategoriaScreen } listeners={({ navigation, route }) => ({
                    tabPress: e => {   
                      setTabSelected('Categorias');   
                      setTabModule('Categorias');
                   
                      }, })} />
        <Tab.Screen name="PedidoScreen" options={{ title:'',unmountOnBlur:true }} component={ PedidoScreen } listeners={({ navigation, route }) => ({
                    tabPress: e => { 
                      setTabSelected('Pedido');  
                      setTabModule('Pedido');
                    // const payload= flags;
                    //   payload.isNotificaciones=false;
                    //   setFlags(payload);

                    //   const payload1 = ids;
                    //   payload1.idOpinionBusqueda= '';
                    //   payload1.idOpinionSeleccionado='';
                    //   setIds(payload1);

                        }, })} />
       

      </Tab.Navigator>
      )
  

 
}


const styles = StyleSheet.create({
  dotNotification:{
    fontSize:75,
    color:  'orange',
    marginTop:-61, 
    right:11
  },
});