import React, { useContext, useEffect } from 'react';
import { createDrawerNavigator, DrawerContentComponentProps, DrawerContentScrollView } from '@react-navigation/drawer';
import { Image, Text, useWindowDimensions, View } from 'react-native';
import { gstyles } from '../theme/appTheme';
import { NavigationHome } from './NavigationHome';
import { NavigationLogin } from './NavigationLogin';
import { OpcionMenuLateral } from '../components/login/OpcionMenuLateral';
import { GeneralContext } from '../state/GeneralProvider';
import RNRestart from 'react-native-restart';
import { Loading } from '../components/Loading';
import CustomIcon from '../theme/CustomIcon';

const Drawer = createDrawerNavigator();


//doc -->  https://docs.swmansion.com/react-native-reanimated/docs/fundamentals/installation/
//yarn add react-native-reanimated@next
// module.exports = {
//     ...
//     plugins: [
//         ...
//         'react-native-reanimated/plugin',
//     ],
// };
//Run pod install in the ios/ directory.


export const NavigationLateral = ( { navigation }:Props) => {

    const { width,height } = useWindowDimensions();
    const { flags } = useContext( GeneralContext )
   
    useEffect(() => {
        
      navigation.setOptions(
          {
              headerShown:false,
              title:'',
          },
          )
    }, [])

 
      if(flags.isLoading){
        return <Loading color='orange'></Loading>
      }
      return (
                <Drawer.Navigator 
                
                  screenOptions={{
                    //swipeEnabled:flags.leftMenuAccesible? true:false,
                    // gestureEnabled:flags.leftMenuAccesible? true:false,
                                  drawerPosition:'left',
                                  headerShown: false, 
                                  drawerType:(width >=768 ? 'permanent' : 'front')  ,
                                  }}  
                  drawerContent={ (props) => <MenuInterno { ...props }></MenuInterno> } >
                  <Drawer.Screen name="NavigationHome" component={ NavigationHome } options={{ title:'', }} initialParams={{screen:'HomeScreen'}}/>      
                  <Drawer.Screen name="NavigationHomeCategoria" component={ NavigationHome } initialParams={{screen:'CategoriaScreen'}}/>
                  <Drawer.Screen name="NavigationHomePedido" component={ NavigationHome } initialParams={{screen:'PedidoScreen'}}/>
                  <Drawer.Screen name="NavigationHomePromocion" component={ NavigationHome } initialParams={{screen:'PromocionScreen'}}/>
                  <Drawer.Screen name="NavigationHomeRelatorio" component={ NavigationHome } initialParams={{screen:'RelatorioScreen'}}/>
                  {/* <Drawer.Screen name="ChangePasswordScreen"  component={ ChangePasswordScreen } /> */}
                  <Drawer.Screen name="NavigationLogin"  component={ NavigationLogin } />
                </Drawer.Navigator>
            );
          
   
}

const MenuInterno = ({navigation}: DrawerContentComponentProps ) =>{
  const { logOut,setTabSelected,setTabModule, usuario,sesion} = useContext( GeneralContext )
  //const { getMonthAgenda } = useAgenda()
  //const {graphMotiveGoNoGo,loadResumoMetrics,graphParticipaciones} = useHome()
  return (
     <DrawerContentScrollView>

       {/* Imagen avatar */}
       {/* <View style={gstyles.avatarContainer}>
          <Image style={gstyles.avatar} 
            source={{ uri:'https://www.caribbeangamezone.com/wp-content/uploads/2018/03/avatar-placeholder-300x300.png' }} >
          </Image>
       </View> */}
       <View  style={gstyles.avatarContainer} >
         <Image style={{justifyContent:'center', alignItems:'center', width:150,height:150}} source={require('../assets/vertical-logo.png')} ></Image>
         </View>

        {/* Opciones del menu */}
        <View style={gstyles.menuContainer}>

          {/* <View style={{flexDirection:'row', paddingBottom:10}}>
            <Text>
            <CustomIcon name={'ic_outline-email'} size={25} color={'black'} ></CustomIcon>
            </Text>
            <Text style={{...gstyles.menuTexto,marginTop:2, paddingLeft:8, fontFamily:'Roboto-Bold', fontSize:15,color:'#757575'}}>{usuario.email}</Text>
          </View>

          <View style={{flexDirection:'row'}}>
            <Text>
            <CustomIcon name={'gridicons_user'} size={25} color={'black'} ></CustomIcon>
            </Text>
            <Text style={{...gstyles.menuTexto,marginTop:2, paddingLeft:8, fontFamily:'Roboto-Bold', fontSize:15,color:'#757575'}}>{sesion.nombre}</Text>
          </View> */}
          
          <OpcionMenuLateral iconName='ic_baseline-home' color='black' label='Home'  onPress={() =>{
            setTabSelected('Logo')
            setTabModule('Logo');
            navigation.navigate('NavigationHome'); 
            }}></OpcionMenuLateral>

          <OpcionMenuLateral iconName='ic_baseline-search' color='black' label='Busqueda' onPress={() =>{
            setTabSelected('Busqueda');
            setTabModule('Busqueda');
           

            navigation.navigate('BusquedaScreen'); 
            }}></OpcionMenuLateral>

          <OpcionMenuLateral iconName='ic_round-interests' color='black' label='Categorias' onPress={() =>{
            setTabSelected('Categorias');
            setTabModule('Categorias');
      
            navigation.navigate('CategoriaScreen'); 
            }}></OpcionMenuLateral>

          <OpcionMenuLateral iconName='ic_sharp-analytics' color='black' label='Pedido' onPress={() =>{
            setTabSelected('Pedido');
            setTabModule('Pedido');

            navigation.navigate('PedidoScreen'); 
            }}></OpcionMenuLateral>

         
                    


          
          
        </View>

     </DrawerContentScrollView>
  );
}