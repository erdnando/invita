import { useContext, useEffect, useState } from 'react';
import { GeneralContext } from '../state/GeneralProvider';
import vistaApi from '../api/vista';
import { GraphMotiveGoNoGo } from '../models/response/GraphMotiveGoNoGo';
import { ResumoMetricsResponse } from '../models/response/ResumoMetrics';
import { GraphParticipaciones } from '../models/response/GraphParticipaciones';
import { TipoUsuario } from '../models/Usuario';



export const useHome =  () => {
   

        const {flags,setFlags, } = useContext( GeneralContext );



        const floading=(valor:boolean)=>{
            const payload= flags;
            payload.isLoadingRelatorio= valor;//isLoadingResumoOportunidades
            
            setFlags(payload);
        }

       

        useEffect(() => {
            console.log('recargando home');
            floading(true)
            // graphMotiveGoNoGo();
            // loadResumoMetrics();
            // graphParticipaciones();
            //floading(false)
           
          }, [])

         
        //exposed objets 
        return {   floading }
}
