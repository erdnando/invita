import { useContext } from 'react';
import { GeneralContext } from '../state/GeneralProvider';
import vistaApi from '../api/vista';
import Toast from 'react-native-toast-message';
import { useNavigation } from '@react-navigation/native';



export const useReset =  () => {

  
    const navigation = useNavigation();
        const { flags, usuario,setUsuario,setFlags,sesion,setTabModule,setTabSelected,ids,setIds } = useContext( GeneralContext );


        const floading=(valor:boolean)=>{
            const payload= flags;
            payload.isLoadingResetSena= valor;
            setFlags(payload);
        }

     

        const changePassword = async()=>{

            if(usuario.nuevoPassword1.trim() != usuario.nuevoPassword2.trim()){
                floading(false)
                Toast.show({type: 'ko',props: { mensaje: 'As senhas não conferem!' }});
                //return false;
            }

            try {

                floading(true)
                console.log('changePassword..')
                console.log(usuario.email)
                console.log(usuario.password)
                console.log(usuario.nuevoPassword1)
                const resp = await vistaApi.post<any>('/services/changePassword',{  
                    "login": usuario.email,
                    "senhaAtual": usuario.password,
                    "senhaNova": usuario.nuevoPassword1 }, {
                                    headers:{
                                    'Content-Type': 'application/json',
                                    'Accept': 'application/json',
                                    "X-Auth-Token": sesion.token
                                },
                });
                console.log('changePassword api');
                console.log(resp);

                const payloadx= usuario;
                payloadx.password=usuario.nuevoPassword1;
                setUsuario(payloadx);

                const payload= flags;
                payload.isPasswordReseted=true;
                payload.isLogedIn=true;
                payload.isLoading=false;
                setFlags(payload);
            
    
                    floading(false)
                    Toast.show({type: 'ok',props: { mensaje: 'Senha atualizada' }});
                    //return true;
                
            } catch (error) {
                console.log('changePassword error..')
                console.log(JSON.stringify(error));
                setTabSelected('Reset de Senha');
                setTabModule('Reset de Senha');
                const payload= flags;
                      payload.isNotificaciones=false;
                      payload.verDetalleAgenda=false;
                      setFlags(payload);
    
                      const payload1 = ids;
                      payload1.idOpinionBusqueda= '';
                      payload1.idOpinionSeleccionado='';
                      payload1.codigoBusqueda='';
                      setIds(payload1);
                     
                  
               

                 const payloadx= flags;
                 payloadx.isLogedIn=false;
                 payloadx.isPasswordReseted=true;
                 setFlags(payloadx);
                
                floading(false)
               // Toast.show({type: 'ko',props: {mensaje:'As senhas não conferem!'} });
               
               // return true;
            }


            
        }
        
        
        //exposed objets 
        return { changePassword, }
}
