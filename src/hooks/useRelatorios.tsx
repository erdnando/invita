import { useContext, useState } from 'react';
import vistaApi from '../api/vista';
import { GeneralContext } from '../state/GeneralProvider';
// import { tipoUsuario } from '../models/enums';
import { Client } from '../models/response/Client';


export const useRelatorios =  () => {

    const { relatorio,setRelatorio,cliente,setCliente,sesion,flags,setFlags } = useContext( GeneralContext );

        const floading=(valor:boolean)=>{
            const payload= flags;
            payload.isLoadingFilterCliente= valor;
           
            setFlags(payload);
        }


        const clearFilterResults = async ()=>{
            let clientes = cliente
            clientes=[]
            setCliente(clientes)
        }
        const onChangeFiltroCliente = async (filtroCliente:string) =>{

            if(filtroCliente.trim().length==0){
                clearFilterResults(); 
                let paylod =relatorio;
                paylod.filtroCliente='';
                setRelatorio(paylod)
                return;
            }

            if(relatorio.omitFilter){
                const payload= relatorio;
                payload.omitFilter=false;
                setRelatorio(payload)
                 return;
             }

             floading(true)

            console.log('dato a buscar')
            console.log(filtroCliente)
            console.log(sesion.token)

             //request data from api
             let clientes = cliente
             clientes=[]
             setCliente(clientes)


            //======================API call==========================
             let urlString = '/services/agency/listByName/'+filtroCliente.trim()+'?charter='+sesion.charter;
             console.log(urlString)
            try {

                const resp = await vistaApi.get<Client[]>(urlString,{
                    headers:{
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        "X-Auth-Token": sesion.token
                    },
                });

                console.log('op listByName:::::::::::::::::::::');
                console.log(resp.data);

                if(resp.data!=undefined){
                    if(resp.data.length>0){
                        resp.data.forEach(function(item,index){
                             
                            clientes.push({
                                "id": item.id,
                                "cnpj": item.cnpj,
                                "razaoSocial": item.razaoSocial,
                                "fantasia": item.fantasia,
                                "contato": item.contato,
                                "contato2": item.contato2,
                                "contato3": item.contato3,
                                "email": item.email,
                                "endereco": item.endereco,
                                "numero": item.numero,
                                "complemento": item.complemento,
                                "bairro": item.bairro,
                                "cep": item.cep,
                                "descricaoCidade": item.descricaoCidade,
                                "descricaoUF": item.descricaoCidade,
                                "status": item.status,
                                "charter": item.charter
                            })
                           });

                           setCliente(clientes)
                    }else{
                        clientes.push({
                            "id": 1,
                            "cnpj": '',
                            "razaoSocial": 'Não há dados',
                            "fantasia": '',
                            "contato": '',
                            "contato2": '',
                            "contato3": '',
                            "email": '',
                            "endereco": '',
                            "numero": '',
                            "complemento": '',
                            "bairro": '',
                            "cep": '',
                            "descricaoCidade": '',
                            "descricaoUF": '',
                            "status": '',
                            "charter": ''
                          })

                          setCliente(clientes)
                     }
                    

                    
                }
                // if(resp.data.razaoSocialCliente !== undefined){
                //     console.log("asignando datos");
                //     // const payload= agenda;
                //     // payload.resumo= resp.data;
                //     // setAgenda(payload);
                // }
                floading(false)

        }catch(error){
            console.log('error al consultar listByName')
            console.log(error.response);
            floading(false)
        }
        //========================================================







          const payload= relatorio;
            payload.filtroCliente=filtroCliente;
            setRelatorio(payload);

           
           
            console.log('show cliente obj::::::::')
            console.log(cliente)
        }


        const onChangeFiltroClienteAux = async (filtroCliente:string) =>{

            if(filtroCliente.trim().length==0){
                clearFilterResults(); 
                let paylod =relatorio;
                paylod.filtroCliente='';
                setRelatorio(paylod)
                return;
            }

            if(relatorio.omitFilter){
                    const payload= relatorio;
                    payload.omitFilter=false;
                    setRelatorio(payload)
                     return;
                 }

       
             //request data from api
             let clientes = cliente
             clientes=[]
             setCliente(clientes)


            const payload= relatorio;
            payload.filtroCliente=filtroCliente;
            setRelatorio(payload);

        }

        
        //exposed objets 
        return {
            onChangeFiltroCliente,clearFilterResults,onChangeFiltroClienteAux
        }
}
