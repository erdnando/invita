import { useContext, useState } from 'react';
import { GeneralContext } from '../state/GeneralProvider';
import vistaApi from '../api/vista';
import { AuthLogin } from '../models/response/AuthLogin';
import picsumApi from '../api/picsum';



export const useImages =  () => {

        const { flags,setFlags } = useContext( GeneralContext );
     

        const floading=(valor:boolean)=>{
            const payload= flags;
            payload.isLoadingImageTalento= valor;
            //payload.isPasswordReseted=valor;
            setFlags(payload);
        }

        const floadingFalse=()=>{
            floading(false)
        }

        const floadingIni=()=>{
            floading(true)
        }

        const getRandomImage = async () =>{

            try {

                floading(true)
                console.log('getRandomImage..')
                // const resp = await picsumApi.get<string>('/200/100',{}, {
                //                     headers:{
                //                     'Content-Type': 'application/json',
                //                     'Accept': 'application/json',
                //                 },
                // });
                const resp = await picsumApi.get<string>('/200/100',{
                    headers:{
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },
                }, 
                );

                console.log(resp);

                floading(false)
                console.log(resp);
               
            } catch (error) {
                console.log('get image error..')
                console.log(error);
                floading(false)
               // Toast.show({type: 'ko',props: {mensaje:error.response.data.message} });//error.response.data.message     'E-mail ou senha não conferem!'
                return false;
            }
            
        }

        
        //exposed objects 
        return { getRandomImage,floading,floadingFalse,floadingIni }
}
