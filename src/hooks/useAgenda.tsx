import { useContext } from 'react';
import { GeneralContext } from '../state/GeneralProvider';
import vistaApi from '../api/vista';
import Toast from 'react-native-toast-message';
import { TipoUsuario } from '../models/Usuario';
import { AgendaItem } from '../models/AgendaItem';
import { OpportunityCustomFindById } from '../models/response/OpportunityCustomFindById';

export const useAgenda =  () => {

        const { ids ,flags,setFlags} = useContext( GeneralContext );

        const floading=(valor:boolean)=>{
            const payload= flags;
            payload.isLoadingParecer= valor;
            payload.isLoadingAgenda=valor;
            setFlags(payload);
        }

        const floadingMonth=(valor:boolean)=>{
            const payload= flags;
            payload.isLoadingMonthAgenda= valor;
            setFlags(payload);
        }

        const lastday = (anio:number,month:number) =>{
            return  new Date(anio, month, 0).getDate();
            }
        
        const getMonthAgenda = async (todayAgenda:Date) =>{
            //floading(true)
            //floadingMonth(true)

            //var today =  new Date();
            console.log('on getMonthAgenda ::::::::::::::::::::::::')
            //console.log(todayAgenda)
            let mesn =  (parseInt(todayAgenda.getMonth().toString())+1 );
            let mess = mesn<10 ? '0'+mesn : mesn.toString()
           
            var yyyy = todayAgenda.getFullYear();
            let ultimodia = lastday(yyyy,mesn)
            //console.log(yyyy +'-'+mess+'-'+'01');
            //console.log(yyyy+'-'+mess+'-'+ultimodia);
            let fechaIni = yyyy +'-'+mess+'-'+'01';
            let fechaFin = yyyy+'-'+mess+'-'+ultimodia;
            let urlString='';
            
                             
            try {
                // if(usuario.tipo==TipoUsuario.USER_TERCEIRO){//si es terciario
                //     console.log('terciario')
                //    urlString='services/calendar/list?dataCertameInicio='+fechaIni+'&dataCertameFim='+fechaFin+'&charter='+sesion.charter+'&colaboradorId=0'+'&clienteId='+sesion.clienteId;
                //    console.log(urlString)
                // }else{
                //    urlString='services/calendar/list?dataCertameInicio='+fechaIni+'&dataCertameFim='+fechaFin+'&charter='+sesion.charter+'&colaboradorId='+sesion.colaboradorId;
                //    console.log('colaborador')
                //    console.log(urlString)
                // }
               
                // const resp = await vistaApi.get<any>(urlString,{
                //     headers:{
                //         'Content-Type': 'application/json',
                //         'Accept': 'application/json',
                //         "X-Auth-Token": sesion.token
                //     },
                // }, 
                // );

          
                //const objx = JSON.parse(JSON.stringify(resp.data));
                //console.log(objx);
                
                let arrResponse = [{}];
                arrResponse=[];

                console.log('filtro asignado:::')
                //console.log(agendaFiltro)

                // Object.getOwnPropertyNames(objx).forEach(function(val, idx, array) {
    
                //     let arr= JSON.parse(JSON.stringify(objx[val])) as AgendaItem[];
  
                //     arr.forEach(function(item,index){
                //         let estaEnFiltro =isInFiltro(item)
                //         let estaEnDateFiltro =isInDateFilter(item)

                //         console.log(item)
                //         console.log(estaEnFiltro)
                //         console.log(estaEnDateFiltro)
                //         console.log('===============')
                //         if(estaEnFiltro && estaEnDateFiltro){
                           
                //             arrResponse.push( { 
                //                 val,
                //                 "cliente":item.cliente, 
                //                 "dataCertame" : item.dataCertame,
                //                 "horaCertame" : item.horaCertame,
                //                 "oportunidadeId" : item.oportunidadeId,
                //                 "orgao" : item.orgao,
                //                 "parecerEstrategico" : item.parecerEstrategico,
                //                 "plataforma" : item.plataforma,
                //                 "registroPreco" : item.registroPreco,
                //                 "status" : item.status,
                //                 "statusEstrategico" : item.statusEstrategico,
                //                 "statusJuridico" : item.statusJuridico,
                //                 });
                //         }else{
                //             console.log('no agregado por filtro...'+item.oportunidadeId)
                //         }
                //     });

                //   });

                arrResponse.push( { 
                    "val":1,
                    "cliente":"item.cliente", 
                    "dataCertame" : "23/07/2022",
                    "horaCertame" : "13:00",
                    "oportunidadeId" :"1000",
                    "orgao" : "item.orgao",
                    "parecerEstrategico" : "item.parecerEstrategico",
                    "plataforma" :" item.plataforma",
                    "registroPreco" : "item.registroPreco",
                    "status" : "item.status",
                    "statusEstrategico" : "item.statusEstrategico",
                    "statusJuridico" : "item.statusJuridico",
                    });


                  
                 if(arrResponse.length > 0){
   
                     var stringMarkedDates={};
                     const red = {key: 'red', color: 'red', selectedDotColor: 'red'};
                     const green = {key: 'green', color: '#48e879', selectedDotColor: '#48e879'};
                     const yellow = {key: 'yellow', color: '#f4ff35', selectedDotColor: '#f4ff35'};
                     const grey = {key: 'grey', color: '#838892', selectedDotColor: '#838892'};
                     const black = {key: 'black', color: '#454A53', selectedDotColor: '#454A53'};
                     let colorDia = {};
                     let textButton=''
                     
                     let coloresExistentes=[{}];
                     let detailItem=[{}];
                   //  coloresExistentes=[];
                  // const payload= agenda;

                  // payload.arrFechasMes=[];

                    // arrResponse.forEach(function(item,index){
                     //   console.log('item')
                       // console.log(item.dataCertame)
                       // payload.arrFechasMes.push(item.dataCertame)


                    //    if( item.parecerEstrategico=='G' && item.statusEstrategico=='F' && item.status=='F') (colorDia= grey, textButton='FINALIZADA');
                    //     else if( item.parecerEstrategico=='G' && item.statusEstrategico=='F') (colorDia= green , textButton='PARECER GO');        //GG
                    //      else if( item.parecerEstrategico=='G' && item.statusEstrategico=='P') (colorDia= green, textButton='PARECER GO');
                    //      else if( item.parecerEstrategico==null && item.statusEstrategico=='P') (colorDia= yellow, textButton='AGUARDANDO PARECER');
                    //      else if( item.parecerEstrategico=='N' && item.statusEstrategico=='P') (colorDia= red,  textButton='PARECER NO GO');
                    //      else if( item.status=='F') (colorDia= grey, textButton='FINALIZADA');
                    //      else ( colorDia= black, textButton='X');

                    //      if( stringMarkedDates[item.val]){
                    //          //color existente, lo va acumulando
                    //         coloresExistentes.push(colorDia)
                    //         detailItem.push(item)
                    //         stringMarkedDates[item.val] = { dots: coloresExistentes,selected: true,selectedColor: 'transparent', selectedTextColor:'black',detailItem:detailItem,textButton:textButton }
                    //      }else{
                    //          //si es la 1a vez, lo agrega
                    //         coloresExistentes=[];
                    //         detailItem=[];
                    //         coloresExistentes.push(colorDia)
                    //         detailItem.push(item)
                    //         stringMarkedDates[item.val] = { dots: coloresExistentes,selected: true,selectedColor: 'transparent', selectedTextColor:'black',detailItem:detailItem,textButton:textButton }
                            
                    //     }
                         
                      //  });
        
                   //payload.markedDates= stringMarkedDates;
                  // setAgenda(payload);
                   floadingMonth(false)
                   //call click del dia seleccionado  TODO
                   console.log('carga despues del filtro:::')
                   //console.log(agenda.selectedDate)
                   //-----------------------------------
                   reloadSelectedDay()


                   //-----------------------------------

                }else{
                    // const payload= agenda;
                    // payload.markedDates= [];
                    // setAgenda(payload);
                }
            
               floadingMonth(false)
            } catch (error) {
                console.log('error al consultar agenda')
               // console.log(error);
                console.log(error.response.data.message)
               // Toast.show({type: 'ko',props: { mensaje: error.response.data.message}});
               floadingMonth(false)
            }
        }

        const reloadSelectedDay=()=>{
            // const payload= agenda;
            // payload.selectedDate = dateString;
            // setAgenda(payload)

            console.log('day click')
           // console.log(agenda.markedDates[agenda.selectedDate] );

            // if(agenda.markedDates[agenda.selectedDate] != undefined){

            //       let arrAux:AgendaItem[] =agenda.markedDates[agenda.selectedDate].detailItem;
            //       const payload= agenda;

            //       //if(arrAux.filter)

            //       if(arrAux.length>0){
            //         payload.lastUpdates=[];
            //         let hora = '';
            //         let horaAnterior = '';
            //         let horaVisible=true;
            //         let colorBoton='black';
            //         let textButton=''
            //         arrAux.forEach(function(item,index){

            //           hora = item.horaCertame.substring(0,2)+':'+item.horaCertame.substring(2);

            //           if(hora==horaAnterior){
            //             horaVisible=false;
            //           }else{
            //             horaVisible=true;
            //           }
            //           horaAnterior=hora;


            //           if( item.parecerEstrategico=='G' && item.statusEstrategico=='F' && item.status=='F') (colorBoton= '#b2b8b7', textButton='FINALIZADA'); 
            //           else if( item.parecerEstrategico=='G' && item.statusEstrategico=='F') (colorBoton= '#48e879', textButton='PARECER GO');        //GG
            //           else if( item.parecerEstrategico=='G' && item.statusEstrategico=='P') (colorBoton= '#48e879', textButton='PARECER GO');
            //           else if( item.parecerEstrategico==null && item.statusEstrategico=='P')( colorBoton= '#f4ff35' ,textButton='AGUARDANDO PARECER');
            //           else if( item.parecerEstrategico=='N' && item.statusEstrategico=='P') (colorBoton= 'red',  textButton='PARECER NO GO');
            //           else if( item.status=='F') (colorBoton= '#b2b8b7', textButton='FINALIZADA');
            //           else ( colorBoton= 'black',textButton='X');
            //           console.log('textButton')
            //           console.log(textButton)

            //           //--------------------------------------------
                     
            //             payload.lastUpdates.push({ 
            //               id: index.toString(),
            //               tipo:'EVENT',
            //               dia:'Hoy',
            //               hora: hora,
            //               descripcion:item.orgao,
            //               color: 'red',
            //               icon:'bx_bxs-message-alt-error',
            //               horaVisible:horaVisible,
            //               oportunidadId:item.oportunidadeId.toString(),
            //               plataforma:item.plataforma,
            //               colorBoton:colorBoton,
            //               textButton:textButton,
            //             });

                   
                      
                     
            //         });
            //       }

            //       setAgenda(payload);
            // }
        }

        const isInFiltro=(item:AgendaItem)=>{
            let status='';
         

            if( item.parecerEstrategico=='G' && item.statusEstrategico=='F' && item.status=='F') (status='FINALIZADA');
            else if( item.parecerEstrategico=='G' && item.statusEstrategico=='F') (status='PARECER GO');        //GG
            else if( item.parecerEstrategico=='G' && item.statusEstrategico=='P') (status='PARECER GO');
            else if( item.parecerEstrategico==null && item.statusEstrategico=='P') (status='AGUARDANDO PARECER');
            else if( item.parecerEstrategico=='N' && item.statusEstrategico=='P') (status='PARECER NO GO');
            else if( item.status=='F') (status='FINALIZADA');

            //console.log(status)

            if(agendaFiltro.bTodo && agendaFiltro.bParecerOK && agendaFiltro.bAguardando  && agendaFiltro.bFinalizado )return true;
            if(agendaFiltro.bParecerOK && agendaFiltro.bAguardando  && agendaFiltro.bFinalizado )return true;

            if(agendaFiltro.bFinalizado && status=='FINALIZADA' )return true;
            if(agendaFiltro.bAguardando && status=='AGUARDANDO PARECER' )return true;
            if(agendaFiltro.bParecerOK && status=='PARECER GO' )return true;


            return false;
        }

        const validaFecha = (fecha:string)=>{
            console.log('fecha para convertir::::::'+fecha)
            let fechaAux =fecha.trim().split('-');
            let mesAux = fechaAux[1];
            console.log('fecha convertida::::::'+mesAux)
            console.log(fechaAux[0] + armaMe2Number(mesAux)+ fechaAux[2])
            return fechaAux[0] +'-'+ armaMe2Number(mesAux)+'-'+ fechaAux[2];
        }

        const isInDateFilter=(item:AgendaItem)=>{
            
            console.log(item.dataCertame)
            console.log(agendaFiltro.filtroFecha)
            console.log(agendaFiltro.filtroHorario)
           console.log('----------------------')
            if(agendaFiltro.filtroFecha=='Selecione uma data' )return true;

           
            let fechaEvento = new Date(item.dataCertame)
           //  fechaEvento.setHours(fechaEvento.getHours() + parseInt(item.horaCertame.substring(0,2)))
            // fechaEvento.setMinutes(fechaEvento.getMinutes() + parseInt(item.horaCertame.substring(2)))
 


            let fechaFiltro = new Date(Date.parse(validaFecha(agendaFiltro.filtroFecha)));
            console.log('fecha filtro:::::')
            console.log(agendaFiltro.filtroFecha)
 
            if(agendaFiltro.filtroHorario != 'Selecione um horário'){

               fechaEvento.setHours(fechaEvento.getHours() + parseInt(item.horaCertame.substring(0,2)))
               fechaEvento.setMinutes(fechaEvento.getMinutes() + parseInt(item.horaCertame.substring(2)))


                let arrHours = agendaFiltro.filtroHorario.split(':')
                fechaFiltro.setHours(fechaFiltro.getHours() + parseInt(arrHours[0]))
                fechaFiltro.setMinutes(fechaFiltro.getMinutes() + parseInt(arrHours[1]))
            }else{
                //no selecciono horario
                //set ambas horas a 0:00
                //fechaFiltro.setHours(23)
                //fechaFiltro.setMinutes(59)

                //fechaEvento.setHours(fechaFiltro.getHours()+24)
               // fechaEvento.setMinutes(fechaFiltro.getMinutes())
            }
           

            console.log('comaparando::')
            console.log(fechaEvento)
            console.log('<=')
            console.log(fechaFiltro)
             if(fechaEvento <= fechaFiltro){
                 return true; 
            }
         
            return false;
        }

        const isIntimeFilter=(horaCertame:string)=>{
            
            console.log(horaCertame)
            console.log(agendaFiltro.filtroHorario)
           console.log('----------------------')
            if( agendaFiltro.filtroHorario=='Selecione um horário' )return true;
           
            let horaFiltro = parseFloat(agendaFiltro.filtroHorario.replace(':',''));
            let horaEvento = parseFloat(horaCertame);

            console.log('comaparando::')
            console.log(horaEvento)
            console.log('<=')
            console.log(horaFiltro)
             if(horaEvento <= horaFiltro)return true;
         


            return false;
        }

        const loadResumo=()=>{

            getResumoTab();
            getInformacionesTab();
            getDocumentosTab();
        }


        const getInformacionesTab = async () =>{
            try {
                    //agenda.selectedOportunidadId
                    const resp = await vistaApi.get<OpportunityCustomFindById>('/services/opportunityCustom/findById/'+agenda.selectedOportunidadId+'/'+sesion.clienteId+'?charter='+ sesion.charter+'&colaboradorId=0',{
                        headers:{
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                            "X-Auth-Token": sesion.token 
                        },
                    }, 
                    );

                    console.log('op resumo:::::::::::::::::::::');
                    console.log(resp.data);

                    
                    if(resp.data.razaoSocialCliente !== undefined){
                        console.log("asignando datos");
                        const payload= agenda;
                        payload.resumo= resp.data;
                        setAgenda(payload);
                    }
                    floading(false)

            }catch(error){
                console.log('error al consultar OpportunityCustomFindById')
                console.log(error);
               // floading(false)
            }
        }

        const getResumoTab = async () =>{
            try {
                    //agenda.selectedOportunidadId
                    const resp = await vistaApi.get<OpportunityCustomFindById>('/services/opportunityCustom/findById/'+agenda.selectedOportunidadId+'/'+sesion.clienteId+'?charter='+ sesion.charter+'&colaboradorId=0',{
                        headers:{
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                            "X-Auth-Token": sesion.token 
                        },
                    }, 
                    );

                    console.log('op resumo:::::::::::::::::::::');
                    console.log(resp.data);

                    
                    if(resp.data.razaoSocialCliente !== undefined){
                        console.log("asignando datos");
                        const payload= agenda;
                        payload.resumo= resp.data;
                        setAgenda(payload);
                    }
                    floading(false)

            }catch(error){
                console.log('error al consultar OpportunityCustomFindById')
                console.log(error);
               // floading(false)
            }
        }

        const getDocumentosTab = async () =>{
            try {
                    //agenda.selectedOportunidadId
                    const resp = await vistaApi.get<DocumentosAgenda[]>('/services/clientDocument/list/opportunity/'+agenda.selectedOportunidadId,{
                        headers:{
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                            "X-Auth-Token": sesion.token 
                        },
                    }, 
                    );

                    console.log('op documentos:::::::::::::::::::::');
                    console.log(resp.data);

                    
                    if(resp.data.length>0){
                        console.log("asignando datos");
                        const payload= agenda;
                        payload.documentos= resp.data;
                        setAgenda(payload);
                    }
                    floading(false)

            }catch(error){
                console.log('error al consultar OpportunityCustomFindById')
                console.log(error);
               // floading(false)
            }
        }
       

        const actualizaAgenda = () =>{

            const payloadx= agenda;
            payloadx.lastUpdates =[];
            setAgenda(payloadx)

            if(!agendaFiltro.bTodo && !agendaFiltro.bParecerOK && !agendaFiltro.bAguardando  && !agendaFiltro.bFinalizado ){

                const payload= flags;
                payload.modalFiltrosVisible =true;
                setFlags(payload)
                Toast.show({type: 'ko',props: { mensaje: 'Seleccione al menos un filtre' }});
            }else{
                //reload calendar with filters
                console.log('recargando calendario')


                getMonthAgenda(agenda.todayAgenda)
                }

                const payload= flags;
                payload.modalFiltrosVisible =false;
                setFlags(payload)
        }

        const armaMesString = (month:string) => {

            switch (month) {
              case '01':
                return 'Janeiro'
                case '02':
                return 'Fevereiro'
                case  '03':
                return 'Março'
                case  '04':
                return 'Abril'
                case  '05':
                return 'Maio'
                case  '06':
                return 'Junho'
                case  '07':
                return 'Julho'
                case  '08':
                return 'Agosto'
                case  '09':
                return 'Setembro'
                case  '10':
                return 'Outubro'
                case  '11':
                return 'Novembro'
                case  '12':
                return 'Dezembro'  
                case  '13':
                return 'Dezembro'
              default:
                return '';
                break;
            }
        }
        
        const add0AlMes =(messs :number) =>{
            switch (messs) {
            case 1:
              return '01'
              case 2:
              return '02'
              case  3:
              return '03'
              case  4:
              return '04'
              case  5:
              return '05'
              case  6:
              return '06'
              case  7:
              return '07'
              case  8:
              return '08'
              case  9:
              return '09'
            default:
              return messs.toString();
              break;
            }
        }

        const armaMe2Number = (month:string) => {

            switch (month) {
              case 'Janeiro' || 'Enero'|| 'January':
                return '01'
                case 'Fevereiro'|| 'Febrero'|| 'February':
                return '02'
                case  'Março'|| 'Marzo'|| 'March':
                return '03'
                case  'Abril'|| 'Abril'|| 'April':
                return '04'
                case  'Maio'|| 'Mayo'|| 'May':
                return '05'
                case  'Junho'|| 'Junio'|| 'June':
                return '06'
                case  'Julho'|| 'Julio'|| 'July':
                return '07'
                case  'Agosto'|| 'Agosto'|| 'August':
                return '08'
                case  'Setembro'|| 'Septiembre'|| 'September':
                return '09'
                case  'Outubro'|| 'Octubre'|| 'October':
                return '10'
                case  'Novembro'|| 'Noviembre'|| 'November':
                return '11'
                case  'Dezembro'|| 'Diciembre'|| 'December':
                return '12'  
              default:
                return month;
                break;
            }
        }
        //exposed objets 
        return {
            getMonthAgenda,loadResumo,floadingMonth,actualizaAgenda
        }
}
        

