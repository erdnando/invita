import { useContext, useState } from 'react';
import { GeneralContext } from '../state/GeneralProvider';
import vistaApi from '../api/vista';
import Toast from 'react-native-toast-message';
import { Flags } from '../models/Flags';


export const useSearch =  () => {

        const { ids ,setIds, flags,setFlags,talentosBusqueda,talentosList,setTalentosBusqueda,talentosMasPopulares,setTalentosMasPopulares} = useContext( GeneralContext );


        const floading=(valor:boolean)=>{
            const payload= flags;
            //payload.isLoading= valor;
         
            payload.isLoadingSearch= valor;
            //payload.resultadosAgendaVisible=valor;
            setFlags(payload);
        }

        
        const applyFilters = async (value1: boolean,value2: boolean,value3: boolean,value4: boolean,value5: boolean)=>{

            const payload = flags;
            payload.valoracion1= value1;
            payload.valoracion2= value2;
            payload.valoracion3= value3;
            payload.valoracion4= value4;
            payload.valoracion5= value5;
            setFlags(payload);
            
        }

        
        const applyOrder = async (value1: boolean,value2: boolean,value3: boolean,value4: boolean)=>{

            const payload = flags;
            payload.orderByMasPoulares= value1;
            payload.orderByMenorMayorPrecio= value2;
            payload.orderByMasRecientes= value3;
            payload.orderByMayorMenorPrecio= value4;
            setFlags(payload);
            
        }
       
        const onChangeSearch = async (busquedaTalento:string) =>{
            const payload= ids;
            payload.busquedaTalento= busquedaTalento;
            setIds(payload);

            if(busquedaTalento.trim()==''){

                let payloadT= talentosBusqueda;
                payloadT=  [];
                setTalentosBusqueda(payloadT);
                return;
            }
          

            let payloadT= talentosBusqueda;
            payloadT=  talentosList.filter(Talento => Talento.talento.toUpperCase().includes(busquedaTalento.trim().toUpperCase()) || 
                                                      Talento.categoria.toUpperCase().includes(busquedaTalento.trim().toUpperCase()) ||
                                                      Talento.descripcion.toUpperCase().includes(busquedaTalento.trim().toUpperCase()    ));


            
            //add defined filters
            if(flags.valoracion1) payloadT =  payloadT.filter(Talento => Talento.valoracion == 5  );//filtro valoracion
            if(flags.valoracion2) payloadT =  payloadT.filter(Talento => Talento.valoracion == 4  );//filtro valoracion
            if(flags.valoracion3) payloadT =  payloadT.filter(Talento => Talento.valoracion == 3  );//filtro valoracion
            if(flags.valoracion4) payloadT =  payloadT.filter(Talento => Talento.valoracion == 2  );//filtro valoracion
            if(flags.valoracion5) payloadT =  payloadT.filter(Talento => Talento.valoracion == 1  );//filtro valoracion

            //mas popular a menor popular
            if(flags.orderByMasPoulares)
               payloadT = payloadT.sort((a,b)=> b.popularidad - a.popularidad ).slice(0,5); //b-a mayor a menor    a-b menor a mayor

            //mas reciente a menor 
            if(flags.orderByMasRecientes)
               payloadT = payloadT.sort((a,b)=> new Date(b.fechaAlta).getTime() - new Date(a.fechaAlta).getTime() ); //b-a mayor a menor    a-b menor a mayor

            //mayor precio a menor 
            if(flags.orderByMayorMenorPrecio)
               payloadT = payloadT.sort((a,b)=> b.costo - a.costo ); //b-a mayor a menor    a-b menor a mayor

             //menor precio a mayor 
             if(flags.orderByMenorMayorPrecio)
               payloadT = payloadT.sort((a,b)=> a.costo - b.costo ); //b-a mayor a menor    a-b menor a mayor
            
            
            setTalentosBusqueda(payloadT);
            
        }

        

        const getPopulares=()=>{
            let payloadT= talentosMasPopulares;
          
             payloadT = talentosList.sort((a,b)=> b.popularidad - a.popularidad ).slice(0,5); //b-a mayor a menor    a-b menor a mayor

            setTalentosMasPopulares(payloadT);
        }

        

        const getResultadoBusqueda = () =>{
            console.log('searching....'+ids.busquedaTalento)
        }


        const onChangeSearchCodigo = async (codigoEvento:string) =>{
            // console.log('buscando...'+busquedaTalento);
            // console.log(busquedaTalento)
             const payload= ids;
             payload.codigoEvento= codigoEvento;
             setIds(payload);
         }
         
         const getResultadoEvento = () =>{
            console.log('searching event....'+ids.codigoBusqueda)
        }

  
        //exposed objets 
        return {
            onChangeSearch,getResultadoBusqueda,onChangeSearchCodigo,getResultadoEvento,getPopulares,applyFilters,applyOrder
        }
}
        

