import axios from 'axios'


const vistaApi = axios.create({
    timeout:18000,
    baseURL:'https://services.sejavista.com.br'
})

//http://ec2-3-86-19-112.compute-1.amazonaws.com:8080/vista-api
//https://services.sejavista.com.br/services
//https://services.sejavista.com.br/services


export default vistaApi;