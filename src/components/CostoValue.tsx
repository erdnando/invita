import React from 'react';
import { Text, View } from 'react-native';
import CustomIcon from '../theme/CustomIcon';


interface Props{
    costoValor:number
  }

export const CostoValue = ( {costoValor}:Props ) => {

  var set: number[] = []
  for (var i = 1; i <= costoValor; i++) {
    set.push(1)
  }

    return   <View style={{flex:1,height:20,flexDirection:'row',justifyContent:'flex-end',width:'100%'}}>
            {set.map(a=>{
                  return   <Text style={{opacity:0.7}} key={Math.random()}  >
                              <CustomIcon  name={'healthicons_money-bag'} size={18} color={'black'} ></CustomIcon>
                          </Text>
                      
              })}
        </View>
}
