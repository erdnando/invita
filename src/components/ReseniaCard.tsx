import React from 'react';
import { Text, View, TextStyle, Image } from 'react-native';
import CustomIcon from '../theme/CustomIcon';
import { CostoValue } from './CostoValue';
import { Spacer } from './Spacer';
import { Stars } from './Stars';

interface Props{
  fontSizeTitulo:number,
  cliente:string,
  fecha:string,
  resenia:string,
  calificacion:number,
  colorTitulo:string,
  colorSubTitulo:string,
  separacion?:number,
  alineacion?:TextStyle['textAlign'],
}

export const ReseniaCard = ( { fontSizeTitulo,cliente,fecha,resenia,calificacion,colorTitulo,colorSubTitulo,separacion=8,alineacion='center' }: Props ) => {
   
     return <View style={{flex:1, flexDirection:'column',width:'85%',backgroundColor:'white',marginLeft:30}}>  

               <View style={{flex:1,flexDirection:'row', justifyContent:'space-between',backgroundColor:'transparent',width:'100%'}}>
                  <View style={{flexDirection:'row'}}>
                     <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSizeTitulo,fontWeight:'700', textAlign:alineacion, marginBottom:5, color:colorTitulo}}>{cliente}</Text>
                     <Text style={{fontFamily:'Roboto-Regular',marginLeft:15, marginEnd:15, fontSize:fontSizeTitulo,fontWeight:'400', textAlign:'left', marginBottom:4, color:'gray'}}>{fecha}</Text>
                  </View>

                  <View style={{flexDirection:'row'}}>
                     <Text style={{fontFamily:'Roboto-Regular', fontSize:12,fontWeight:'600', textAlign:alineacion,color:colorSubTitulo,}}>{calificacion+'.0'}</Text>
                     <Image style={{ alignItems: 'center', width: 12,height:10,marginTop:2,marginRight:2,marginLeft:5 }}
                           source={{uri:'https://icon-library.com/images/yellow-star-icon/yellow-star-icon-19.jpg' }}></Image>
                  </View>

               </View>

               <View style={{flex:1,alignContent:'center', width:'90%'}}>
                  <Text style={{fontFamily:'Roboto-Regular',marginEnd:15 , fontSize:fontSizeTitulo,fontWeight:'400', textAlign:'justify', marginBottom:4, color:'black'}}>{resenia}</Text>
               </View>
              

            
            </View>
          
}
