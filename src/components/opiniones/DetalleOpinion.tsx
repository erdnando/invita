import React, { useContext } from 'react';
import { Platform, ScrollView, useWindowDimensions, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { GeneralContext } from '../../state/GeneralProvider';
import { DetallHeaderOpiniones } from './DetalleHeaderOpiniones';
import { DetalleContentOpinion } from './DetalleContentOpinion';
import KeyboardWrapper from '../KeyboardWrapper';
import { TipoUsuario } from '../../models/Usuario';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';



interface Props{
  tipoUsuario:string,
}
// 1 terciario, 2colaborador
export const DetalleOpinion = ( { tipoUsuario}: Props ) => {

  const { height,width } = useWindowDimensions()
     const { ids,sesion,usuario } = useContext( GeneralContext )
     const { top } = useSafeAreaInsets();

     const bodyMartillo =( <View style={ {  marginTop: top, flex:1, alignItems:'center',}}>
                   
                        <View style={{height:136,width:'100%', backgroundColor:'#838892',  }}>
                          <DetallHeaderOpiniones />
                        </View>

                      
                        <View style={{flex:1, width:'100%', backgroundColor:'#838892',justifyContent:'center',alignContent:'center',
                                      alignItems:'center', paddingTop:10,paddingBottom:10}}>
                            
                            {/* TODO add backgound */}
                            <View style={{flex:1,width:'92%',justifyContent:'center',  backgroundColor:'transparent',
                                          alignContent:'center',alignItems:'center' }}>
                                <DetalleContentOpinion></DetalleContentOpinion>
                            </View>
                        </View>
                        
                    </View>)

    if(ids.idMenuOpinionSelected !=3){
      console.log('sin keyboard managment')
      console.log(usuario.tipo)
      console.log(ids.idMenuOpinionSelected)

      if(usuario.tipo==TipoUsuario.COLABORADOR && (ids.idMenuOpinionSelected==1 || ids.idMenuOpinionSelected==2)   ){
        //------------------------------------------------------
        return (<KeyboardAwareScrollView style={{ width:'100%', height:'100%', flex:1, }} bounces={false} contentContainerStyle={{flexGrow: 1}}>
        <ScrollView contentContainerStyle={{flexGrow: 1}} bounces={false}>
                        <View style={ {  flex:1,top:top,height:height <= 750 ? Platform.OS == 'ios' ? height :height-45 : Platform.OS == 'ios' ? height-150 : height-900}}>
                          
                          <View style={{height:136,width:'100%', backgroundColor:'#838892',  }}>
                            <DetallHeaderOpiniones />
                          </View>

                        
                          <View style={{flex:1, width:'100%', backgroundColor:'#838892',justifyContent:'center',alignContent:'center',alignItems:'center', paddingTop:10,paddingBottom:10,height:480}}>
                    
                              <View style={{flex:1,width:'92%',justifyContent:'center',  backgroundColor:'transparent',alignContent:'center',alignItems:'center' }}>
                                  <DetalleContentOpinion></DetalleContentOpinion>
                              </View>
                          </View>
                          
                      </View>
                      </ScrollView>
        </KeyboardAwareScrollView>
)
        //------------------------------------------------------
      }else
       return (     bodyMartillo)
     }
    else {
      console.log('con keyboard managment')
      return (<KeyboardAwareScrollView style={{ width:'100%', height:'100%', flex:1, }} bounces={false} contentContainerStyle={{flexGrow: 1}}>
      <ScrollView contentContainerStyle={{flexGrow: 1}} bounces={false}>
                <View style={ {  flex:1,top:top}}>
                   
                   <View style={{height:136,width:'100%', backgroundColor:'#838892',  }}>
                     <DetallHeaderOpiniones />
                   </View>

                 
                   <View style={{flex:1, width:'100%', backgroundColor:'#838892',justifyContent:'center',alignContent:'center',alignItems:'center', paddingTop:10,paddingBottom:10,height:480}}>
             
                       <View style={{flex:1,width:'92%',justifyContent:'center',  backgroundColor:'transparent',alignContent:'center',alignItems:'center' }}>
                           <DetalleContentOpinion></DetalleContentOpinion>
                       </View>
                   </View>
                   
               </View>
               </ScrollView>
        </KeyboardAwareScrollView>
    )}
}
