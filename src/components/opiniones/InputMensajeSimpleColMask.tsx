import React, { useContext } from 'react';
import { KeyboardType, KeyboardTypeOptions, Text, TextInput, View } from 'react-native';
import { colores } from '../../theme/appTheme';
import { GeneralContext } from '../../state/GeneralProvider';
import { LabelTexto } from './LabelTexto';
import MaskInput, { createNumberMask, Masks } from 'react-native-mask-input';


interface Props{
  placeholder:string;
  campo:string;
  width?:string,
  maxLength?:number,
  keyboardType?:KeyboardType,
  onChangeMensaje: (msg:string) =>void;
  onFocus:() =>void;
}

export const InputMensajeSimpleColMask = ( { placeholder,campo,width='90%',maxLength=8,keyboardType='default' ,onChangeMensaje,onFocus}: Props ) => {

  let colorIcono = colores.primary;

  const decimalMask = createNumberMask({
    delimiter: '.',
    separator: ',',
    precision: 2,
  })

  const decimalMask2 = createNumberMask({
    prefix:[],
    delimiter: '.',
    separator: ',',
    precision: 2,
  })
  
    return (<View style={{flex:1,width:'100%',backgroundColor:'transparent',height:30}}>
              <View style={{marginLeft:15}}>
                 <LabelTexto  fontSize={12} color='#838892' label='' value={placeholder}></LabelTexto>
              </View>
              <MaskInput
                  style={{
                      textAlign:'justify',
                      fontFamily:'Roboto-Regular',
                      fontSize:14,
                      maxHeight:50,
                      width:width,
                      paddingLeft:14,
                      marginTop:0,
                      left:0,
                      margin: 2,
                      borderWidth: 1,
                      paddingBottom:0,
                      borderLeftWidth:0,
                      borderRightWidth:0,
                      borderTopWidth:0,
                      borderColor:campo===''?'black':colorIcono,
                      color:'black'
                  }}
                  mask={decimalMask2}
                  onChangeText={ onChangeMensaje }
                  placeholder={placeholder}
                  placeholderTextColor={'#757575'}
                  placeholderFillCharacter = '_'
                  obfuscationCharacter= '_'
                  keyboardType= {keyboardType}
                  autoCapitalize='none'
                  autoCorrect = {false}
                  multiline= {false}
                  numberOfLines={2}
                  maxLength={maxLength}
                  value={campo}
                  onFocus={ onFocus}
              />
              </View>
    )
}
