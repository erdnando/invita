import React from 'react';
import { Text, View, TextStyle } from 'react-native';
import { Spacer } from './Spacer';

interface Props{
  fontSizeTitulo:number,
  fontSizeSubtitulo:number,
  labelTitulo:string,
  labelSubTitulo:string,
  colorTitulo:string,
  colorSubTitulo:string,
  separacion?:number,
  tituloMarginTop?:number,
  alineacion?:TextStyle['textAlign'],
}

export const LabelResumen = ( { fontSizeTitulo,fontSizeSubtitulo, labelTitulo,labelSubTitulo,colorTitulo,colorSubTitulo,separacion=8,alineacion='center',tituloMarginTop=0 }: Props ) => {



     return <View style={{ width:200,marginLeft:30 }}>   
               <Text style={{fontFamily:'Roboto-Regular',marginTop:tituloMarginTop, fontSize:fontSizeTitulo,fontWeight:'700', textAlign:alineacion, marginBottom:0, color:colorTitulo}}>{labelTitulo}</Text>
               <Spacer height={separacion}></Spacer>
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSizeSubtitulo,fontWeight:'400', textAlign:alineacion,color:colorSubTitulo,}}>{labelSubTitulo}</Text>
            </View>
          
}
