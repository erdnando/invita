import React, { useContext, useState } from 'react';
import {  Platform, Text, TextInput, TouchableOpacity, View, ViewStyle } from 'react-native';
import { colores } from '../../theme/appTheme';
import CheckBox from '@react-native-community/checkbox';
import { CostoValue } from '../CostoValue';
import { Stars } from '../Stars';



interface Props{
  disabled:boolean,
  backgroundColor:ViewStyle['backgroundColor'],
  color:string,
  tittle:string,
  onPress: () => void;
  }

export const TouchableButton = ({ backgroundColor,color,disabled,tittle,onPress} : Props) => {

    return    <TouchableOpacity activeOpacity={0.2}   disabled={disabled}  style={{backgroundColor:backgroundColor,height:45,width:160,flex:0,shadowColor: "black",
                  borderRadius:9, marginHorizontal:18, shadowOpacity: 0.4,shadowOffset: {
                  height: 1, width: 1 },}} onPress= {onPress}>

              <View style={{flex:1, alignContent:'center',alignItems:'center',justifyContent:'center',  borderRadius:9,
              borderColor:'black' }}>
              <Text style={{ fontWeight:'600',color:color,fontSize:13}}>{tittle}</Text>
              </View>

              </TouchableOpacity>
              }
