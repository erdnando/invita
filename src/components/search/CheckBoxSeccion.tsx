import React, { useContext, useState } from 'react';
import {  View } from 'react-native';
import { useSearch } from '../../hooks/useSearch';
import { GeneralContext } from '../../state/GeneralProvider';
import { CheckBoxFiltro } from './CheckBoxFiltro';



// interface Props{
//   campo:boolean,
//   label:string
//   onValueChange: (value:boolean) => void;
//   }

export const CheckBoxSeccion = () => {
  const { flags,setFlags  } = useContext(GeneralContext);
  const { applyFilters } = useSearch();
  // const [toggleCheckBox, setToggleCheckBox] = useState(false)
  // let colorIcono = colores.primary;
  const defaultValor=false;
 

    return    <View style={{flex:0,marginHorizontal:10}}>
                        <CheckBoxFiltro campo={flags.valoracion1} estrellas={5} 
                                onValueChange={(value)=>{

                                      applyFilters(value,defaultValor,defaultValor,defaultValor,defaultValor)
                                        
                                }} />
                        <CheckBoxFiltro campo={flags.valoracion2}  estrellas={4}  
                                onValueChange={(value)=>{
                                        applyFilters(defaultValor,value,defaultValor,defaultValor,defaultValor)
                                
                        }}/>
                        <CheckBoxFiltro campo={flags.valoracion3}  estrellas={3}  
                                onValueChange={(value)=>{
                                        applyFilters(defaultValor,defaultValor,value,defaultValor,defaultValor)
                                
                        }} />
                        <CheckBoxFiltro campo={flags.valoracion4}  estrellas={2}  
                                onValueChange={(value)=>{
                                        applyFilters(defaultValor,defaultValor,defaultValor,value,defaultValor)
                                
                        }}/>
                        <CheckBoxFiltro campo={flags.valoracion5}  estrellas={1}  
                                onValueChange={(value)=>{
                                        applyFilters(defaultValor,defaultValor,defaultValor,defaultValor,value)
                                
                        }}/>
                </View>
}
