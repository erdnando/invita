import CheckBox from '@react-native-community/checkbox';
import React, { useContext, useState } from 'react';
import {  Platform, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { colores } from '../../theme/appTheme';
//import CheckBox from '@react-native-community/checkbox';
import { CostoValue } from '../CostoValue';
import { Stars } from '../Stars';



interface Props{
  campo:boolean,
  estrellas:number,
  onValueChange: (value:boolean) => void;
  }

export const CheckBoxFiltro = ({ estrellas,campo,onValueChange} : Props) => {

    return    <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:13,marginBottom:15, }}>

                <View style={{flex:1,width:'100%',backgroundColor:'transparent',justifyContent:'space-between',alignContent:'center',alignItems:'center', top:5,}}>
                   <Stars costoValor={estrellas}></Stars>
                </View>

                <CheckBox
                  disabled={false}
                  
                  value={campo}
                  tintColors={{true:'#110331', false:'#110331'}}
                  onValueChange={onValueChange}
                />
              
            </View>
}
