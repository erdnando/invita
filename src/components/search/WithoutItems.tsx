import React, { useContext } from 'react';
import { Dimensions, Image, Text, View } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { ButtonSimple } from '../home/ButtonSimple';
import { LabelTitulo } from '../LabelTitulo';
import { Spacer } from '../Spacer';



interface Props{
  label:string,
}

export const WithoutItems = ( { label,}: Props ) => {

 
  const windowWidth = Dimensions.get('window').width;
  const { flags,setFlags } = useContext( GeneralContext )
   
   return <View style={{ flex:1,backgroundColor:'transparent',paddingTop:0, paddingHorizontal:8,alignContent:'center',alignItems:'center' }}>
        
        <Spacer height={30}></Spacer>
        <View  ><Image style={{width:270,height:270}} source={require('../../assets/noData.png')} ></Image></View>
       
        <Spacer height={10}></Spacer>

            <LabelTitulo fontSizeTitulo={20} fontSizeSubtitulo={17} labelTitulo={'¡Ups!'} 
                         labelSubTitulo={'No hemos encontrado resultados'} colorTitulo={'black'} colorSubTitulo={'black'} ></LabelTitulo>
           
           <Spacer height={30}></Spacer>
            {/* <View style={{alignItems:'center'}}>
                <ButtonSimple  backgroundColor='#110331' color='white' label={'Restablecer'} textDecorationLine={'none'} onPress={ ()=> {
                      console.log('siguiente 02 ')
                   
                } }></ButtonSimple>
               

               
            </View> */}

        </View> 

}
