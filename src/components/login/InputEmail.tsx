import React, { useContext } from 'react';
import { NativeSyntheticEvent, Text, TextInput, TextInputSubmitEditingEventData, TouchableOpacity, View } from 'react-native';
import { useLogin } from '../../hooks/useLogin';
import { GeneralContext } from '../../state/GeneralProvider';
import CustomIcon from '../../theme/CustomIcon';
import { colores } from '../../theme/appTheme';


interface Props{
  modo:string,
  label:string,
  iconLeft:string,
  placeHolderTextColor:string,
  iconRight:string,
  onSubmitEditing?: ((e: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => void) | undefined
}

export const InputEmail = ( { placeHolderTextColor, modo, label, iconLeft ,iconRight,onSubmitEditing}: Props ) => {

  let colorIcono = colores.primary;

    //invoke global state
    const { usuario,setUsuario, flags,setFlags } = useContext( GeneralContext )
    const { onChangeEmail } = useLogin(); 

    
    return (
      <View style={{flexDirection: 'row',left:-8 }}>
      <View style={{flexDirection:'column', }}>
          <Text style={{ left:52,top:5, color: (usuario.email==='')?'transparent':colorIcono,  }}>{label}</Text>
          <CustomIcon  name={iconLeft} size={24} color={usuario.email===''?'black':colorIcono }  style={{left:58, top:5,}} ></CustomIcon>
      </View>
     
          <TextInput
              style={{
                  fontFamily:'Roboto-Regular',
                  height: 40,
                  width:'78%',
                  margin: 12,
                  paddingLeft:45,
                  borderWidth: 1,
                  borderLeftWidth:0,
                  borderRightWidth:0,
                  borderTopWidth:0,
                  color:'black',
                  borderColor:usuario.email===''?'black':colorIcono,
              }}
              onChangeText={ onChangeEmail }
              placeholder={label}
              placeholderTextColor={ placeHolderTextColor='#A3A4A6' }
              keyboardType='email-address'
              autoCapitalize='none'
              autoCorrect = {false}
              maxLength={33}
              value={usuario.email}
              onSubmitEditing={onSubmitEditing}
          />
          <TouchableOpacity style={{right:45, top:20}} onPress={() =>{ 

            const payload= usuario;
            payload.email='';
            setUsuario(payload)
          
           
            const payload2= flags;
            //payload2.isAlertLoginVisible=false;
            setFlags(payload2);



            }}>
              <Text>
                  <CustomIcon  name={iconRight} size={24} color='#000000'  ></CustomIcon>
              </Text>
          </TouchableOpacity>
     
  </View>
    )
}
