
import React, { useContext } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { AgendaItem } from '../models/AgendaItem';
import { GeneralContext } from '../state/GeneralProvider';


interface Props{
  dateString:string;
  day:number ;
  dayState:string;
  }

export const DayComponent = ({ dateString,day,dayState}:Props ) => {

  const { talentoSelected,setTalentoSelected } = useContext( GeneralContext )
 // console.log(dateString)
 // console.log(new Date(dateString).getDay()+1)
  let currentDay=new Date(dateString).getDay()+1;
  let diaDisponible = talentoSelected.contratacion.disponibilidad.find(element => element == currentDay);
  let diaSeleccionado = talentoSelected.diasSolicitados.find(element => element == dateString);
  //console.log(diaDisponible)

  return (

   (diaDisponible != undefined ?
    <View style={{borderWidth: ( diaSeleccionado == undefined ? 0: 4) ,borderColor: ( diaSeleccionado == undefined ? 'transparent': '#49218D'), 
                 borderRadius:5, padding:2,backgroundColor: ( diaSeleccionado == undefined ? 'transparent': '#49218D')}}>

           <TouchableOpacity  onPress={() =>{   
                console.log('click...')
                //console.log(dateString)
                const found = talentoSelected.diasSolicitados.find(element => element == dateString);
                if(found==undefined){
                    console.log('Agregando fecha:::'+dateString )
                    const payload= talentoSelected;
                    payload.diasSolicitados.push(dateString)
                    setTalentoSelected(payload)
                }else{
                    console.log('remove selected')

                    const payload= talentoSelected;
                    var filteredArray = payload.diasSolicitados.filter(function(e) { return e !== dateString })
                    payload.diasSolicitados=filteredArray;
                    setTalentoSelected(payload)

                
                }
                console.log("talentoSelected.diasSolicitados")
                console.log(talentoSelected.diasSolicitados)
                          
             }}>
                <Text style={{textAlign: 'center',fontSize:14,fontWeight:( diaSeleccionado == undefined ? '400': 'bold'), color: dayState === 'disabled' ? '#E2E5EA' : ( diaSeleccionado == undefined ? 'black': 'white')}}>{day}</Text>
           </TouchableOpacity>
    </View> :
    <View>
       <Text style={{textAlign: 'center',fontSize:14, color:'#E2E5EA'}}>{day}</Text>
    </View>)
)

   
           
}
