import React from 'react';
import { Text, View, TextStyle, Image } from 'react-native';
import CustomIcon from '../theme/CustomIcon';
import { CostoValue } from './CostoValue';
import { Spacer } from './Spacer';
import { Stars } from './Stars';

interface Props{
  fontSizeTitulo:number,
  talento:string,
  descripcion:string,
  categoria:string,
  calificacion:number,
  costoValor:number,
  colorTitulo:string,
  colorSubTitulo:string,
  separacion?:number,
  alineacion?:TextStyle['textAlign'],
}

export const TalentoBusqueda = ( { fontSizeTitulo,talento,descripcion,categoria,calificacion,costoValor,colorTitulo,colorSubTitulo,separacion=8,alineacion='center' }: Props ) => {

  // const arr = [1,2,3,4,5]
   
     return <View style={{flexDirection:'column',backgroundColor:'transparent'}}>  

               <View style={{flex:1,justifyContent:'flex-start',backgroundColor:'transparent',width:'86%'}}>
                  <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSizeTitulo,fontWeight:'700', textAlign:alineacion, marginBottom:5, color:colorTitulo}}>{talento}</Text>
                  <Text style={{fontFamily:'Roboto-Regular',marginEnd:15, fontSize:fontSizeTitulo,fontWeight:'400', textAlign:'left', marginBottom:4, color:'gray'}}>{descripcion}</Text>
               </View>
              

              {/* barra calif, categoria y costo */}
               <View style={{flexDirection:'row',width:'80%', }}>

                  <Image style={{ alignItems: 'center', width: 12,height:10,marginTop:2,marginRight:2 }}
                        source={{uri:'https://icon-library.com/images/yellow-star-icon/yellow-star-icon-19.jpg' }}></Image>

                  <Text style={{fontFamily:'Roboto-Regular', fontSize:12,fontWeight:'600', textAlign:alineacion,color:colorSubTitulo,}}>{calificacion}</Text>
                  <View style={{width:10}}></View>
                  <Text style={{fontFamily:'Roboto-Regular', fontSize:12,fontWeight:'400', textAlign:alineacion,color:colorSubTitulo,}}>{categoria}</Text>

                 
                 {/* <Stars costoValor={costoValor}></Stars> */}
                 <CostoValue costoValor={costoValor}></CostoValue>
                
               </View>
            
            </View>
          
}
