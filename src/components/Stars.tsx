import React from 'react';
import { Image, Text, View } from 'react-native';
import CustomIcon from '../theme/CustomIcon';


interface Props{
    costoValor:number
  }

export const Stars = ( {costoValor}:Props ) => {

  var set: number[] = []
  for (var i = 1; i <= 5; i++) {
    set.push(i)
  }

    return   <View style={{flex:0,marginLeft:-114, height:37,flexDirection:'row',width:210,
                          borderWidth:1,borderRadius:10,borderColor:'grey',borderStyle:'dotted',}}>
            {set.map(a=>{
              if( costoValor < a)
                {  return   <Image key={Math.random()} style={{ alignItems: 'center', width: 16,height:16,marginTop:7,marginRight:18,marginLeft:8, opacity:0.3 }}
                                   source={{uri:'https://icon-library.com/images/yellow-star-icon/yellow-star-icon-19.jpg' }}></Image>}
              else{
                   return   <Image key={Math.random()} style={{ alignItems: 'center', width: 16,height:16,marginTop:7,marginRight:18,marginLeft:8, }}
                                   source={{uri:'https://icon-library.com/images/yellow-star-icon/yellow-star-icon-19.jpg' }}></Image>
                  }

                      
              })}
        </View>
}
