import React from 'react';
import { Text, View, TextStyle, FlexStyle } from 'react-native';
import { Spacer } from './Spacer';

interface Props{
  fontSizeTitulo:number,
  fontSizeSubtitulo:number,
  labelTitulo:string,
  labelSubTitulo:string,
  colorTitulo:string,
  colorSubTitulo:string,
  separacion?:number,
  marginHorizontal?:number,
  marginBottom?:number,
  marginTop?:number,
  alineacion?:TextStyle['textAlign'],
  fontWeight?:TextStyle['fontWeight'],
  fontWeight2?:TextStyle['fontWeight'],
  width?:FlexStyle['width']
}



export const LabelTitulo = ( { fontSizeTitulo,fontSizeSubtitulo, labelTitulo,labelSubTitulo,colorTitulo,colorSubTitulo,separacion=8,alineacion='center', width='85%',
                               fontWeight='700',fontWeight2='400'   , marginHorizontal=30,marginBottom=0,marginTop=0 }: Props ) => {
     return <View style={{ width:width,marginHorizontal:marginHorizontal,marginBottom:marginBottom,marginTop:marginTop,}}>   
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSizeTitulo,fontWeight:fontWeight, textAlign:alineacion, marginBottom:3, color:colorTitulo}}>{labelTitulo}</Text>
               <Spacer height={separacion}></Spacer>
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSizeSubtitulo,fontWeight:fontWeight2, textAlign:alineacion,color:colorSubTitulo,}}>{labelSubTitulo}</Text>
            </View>
          
}
