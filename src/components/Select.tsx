import React, { useContext, useState } from 'react';
import { StyleSheet, View, Platform, Dimensions } from 'react-native';
import RNPickerSelect, { Item } from 'react-native-picker-select';
import CustomIcon from '../theme/CustomIcon';


interface Props{
  placeholder:string,
  onValueChange: (value: any, index: number) => void,
  items:Item[],
  campo:string,
  width?:string,
  disabled?:boolean,
}

export const Select = ( { onValueChange,items,placeholder,campo,width='87%',disabled=false}: Props ) => {

  //const windowWidth = Dimensions.get('window').width;

    return (
      <View style={{ flexDirection: 'row',left:0, borderWidth:1,width:'90%',borderRadius:6,height:35,
                     borderBottomColor: campo !=null ? 
                    (disabled ? 'grey' : 'green')
                      : 'grey' }}>

              <RNPickerSelect disabled={disabled}
              style={pickerSelectStyles}
                value={campo} 
                placeholder={{label:placeholder, value:""}}
                useNativeAndroidPickerStyle={true}
                onValueChange={onValueChange}
                items={items}
                />
                
                { Platform.OS=='ios' ? <View style={{position:'absolute' }}>
                                        <CustomIcon  name='ic_baseline-keyboard-arrow-down' size={33} color='#838892'   style={{flex:0 ,left:127, position:'absolute', top:0}} ></CustomIcon>
                                       </View>:<View></View>
               }
      </View>
    )
}


const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    flex:1,
    width:250,
      fontSize: 14,
      paddingVertical: 15,
      paddingHorizontal: 10,
      borderWidth: 0,
      color: 'black',
      paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    flex:1,
    width:350,
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 18,
    borderWidth: 0,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  });