import React, { useContext } from 'react';
import {  Keyboard, TouchableWithoutFeedback, View } from 'react-native';
import { Spacer } from '../Spacer';
import { SeccionFiltros } from './SeccionFiltros';
import { RoundedSelectors } from './RoundedSelectors';
import { Charts } from './Charts';
import { ScrollView } from 'react-native-gesture-handler';
import { useRelatorios } from '../../hooks/useRelatorios';
import { GeneralContext } from '../../state/GeneralProvider';
import { TipoUsuario } from '../../models/Usuario';


export const Participaciones = ( ) => {

  const { clearFilterResults } = useRelatorios()
  const {  usuario,cliente } = useContext( GeneralContext )

    return  <TouchableWithoutFeedback onPress={ () => { console.log('clicking');Keyboard.dismiss();clearFilterResults() } }>
    <View style={{flex:1,backgroundColor:'#BCC1CB', width:'100%',  alignItems:'center',alignContent:'space-between',justifyContent:'flex-start' }}>
                {/* filtros    */}
                {/* <ScrollView style={{width:'100%'}}> */}
                  <SeccionFiltros></SeccionFiltros>
                  <Spacer height={20}></Spacer>
                <ScrollView style={{width:'100%'}}>
                {/* botones tabs rounded */}
                 <RoundedSelectors></RoundedSelectors> 
                  <Spacer height={10}></Spacer>
                  <Charts></Charts>
            
               
                  <Spacer height={0}></Spacer>
                </ScrollView>
                 
                  
            </View>
            </TouchableWithoutFeedback>
}
