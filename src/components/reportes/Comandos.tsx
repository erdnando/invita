import React, { useContext } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { colores } from '../../theme/appTheme';
import { GeneralContext } from '../../state/GeneralProvider';
import { useHome } from '../../hooks/useHome';
import { useNavigation } from '@react-navigation/native';



export const Comandos = () => {


 //invoke global state
 const {  relatorio,setRelatorio } = useContext( GeneralContext )
 const {graphParticipaciones,graphMotiveGoNoGo } = useHome( );

    return    <View style={{flexDirection:'row',justifyContent:'flex-end', top:35, }}>
                    <TouchableOpacity  style={{ borderRadius: 100,  }} onPress={()=>{  
                      // limpiar
                      const payload= relatorio;
                      payload.filtroFechaInicial='';
                      payload.filtroFechaFinal='';
                      payload.filtroCliente='';
                      payload.filtroClienteId=0;
                      setRelatorio(payload);

                      //reload graphs
                      graphMotiveGoNoGo();
                     // loadResumoMetrics();
                      graphParticipaciones();

                      }}>
                      <Text  style={{ fontFamily:'Roboto-Regular',fontSize:15,fontWeight:'600', textAlign:'center',color:colores.primary }}>LIMPAR</Text>
                    </TouchableOpacity>

                    <View style={{width:50}}></View>

                    <TouchableOpacity  style={{ borderRadius: 100,  }} onPress={ async()=>{ 
                      //filtrar
                      graphMotiveGoNoGo();
                     // loadResumoMetrics();
                      graphParticipaciones();
                      }} >
                      <Text  style={{ fontFamily:'Roboto-Regular',fontSize:15,fontWeight:'600', textAlign:'center',color:colores.primary }}>FILTRAR</Text>
                    </TouchableOpacity>

                    <View style={{width:10}}></View>
               </View>

      
}
