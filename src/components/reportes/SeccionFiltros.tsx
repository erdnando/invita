import React, { useContext } from 'react';
import { Platform, View } from 'react-native';
import { colores } from '../../theme/appTheme';
import Collapsible from 'react-native-collapsible';
import { GeneralContext } from '../../state/GeneralProvider';
import { FechaInput } from './FechaInput';
import { TopFilter } from './TopFilter';
import { Filtros } from './Filtros';
import { Comandos } from './Comandos';
import { TipoUsuario } from '../../models/Usuario';


export const SeccionFiltros = () => {


  let colorIcono = colores.primary;
 //invoke global state
 const {  relatorio,usuario,cliente } = useContext( GeneralContext )


    return   <View style={{flex:0, width:'100%',justifyContent:'flex-start', backgroundColor:'white'}}>
                 {/* colapsar filtro */}
                 <TopFilter></TopFilter>

                {/* https://github.com/oblador/react-native-collapsible */}
                <Collapsible collapsed={relatorio.isFilterCollapsed} >
                    <View style={{ width:'100%',height:usuario.tipo==TipoUsuario.USER_TERCEIRO ? 120: 150, backgroundColor:'transparent', paddingHorizontal:18,}} >
                            {/* filtro cliente */}
                            <Filtros></Filtros>
                            {/* commands to clean and filter */}
                           {  (cliente.length==0 ) && <Comandos></Comandos>}
                    </View>
                </Collapsible>

              </View>

      
}
