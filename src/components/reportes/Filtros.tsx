import React, { useContext, useEffect, useState } from 'react';
import {  FlatList, Keyboard, Pressable, SafeAreaView, StyleSheet, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import { useRelatorios } from '../../hooks/useRelatorios';
import { colores } from '../../theme/appTheme';
import { Spacer } from '../Spacer';
import { GeneralContext } from '../../state/GeneralProvider';
import { FechaInput } from './FechaInput';
import { TipoUsuario } from '../../models/Usuario';
//import Autocomplete from 'react-native-autocomplete-input';

import { useHome } from '../../hooks/useHome';
import { Loading } from '../Loading';
import { useDebouncedCliente } from '../../hooks/useDebouncedCliente';

export const Filtros = () => {

   
    let colorIcono = colores.primary;
    //invoke global state
    const { relatorio,setRelatorio,usuario,cliente,flags } = useContext( GeneralContext )
    const { onChangeFiltroCliente,onChangeFiltroClienteAux,clearFilterResults } = useRelatorios(); 
    const  debouncedValue  = useDebouncedCliente(relatorio.filtroCliente,500);

    useEffect(() => {
      //console.log('****'+debouncedValue)
      onChangeFiltroCliente(debouncedValue)
    }, [debouncedValue])
    

//https://www.youtube.com/watch?v=Dp8cQU2OcFU
    return    <View >
                        {usuario.tipo==TipoUsuario.COLABORADOR && 
                        <>
                            <TextInput 
                            style={{
                                                fontFamily:'Roboto-Regular',
                                                fontSize:16,
                                                height: 40,
                                                width:'100%',
                                                margin: 1,
                                                paddingLeft:1,
                                                borderWidth: 1,
                                                borderLeftWidth:0,
                                                borderRightWidth:0,
                                                borderTopWidth:0,
                                                borderColor:relatorio.filtroCliente===''?'black':colorIcono,
                                                color:'black',
                                            }}
                            onChangeText={ onChangeFiltroClienteAux }
                            value={relatorio.filtroCliente}
                            placeholder='Cliente'
                            placeholderTextColor={'#757575'}
                            keyboardType='web-search'
                            autoCapitalize='none'
                            autoCorrect = {false}
                            clearButtonMode='while-editing'
                            maxLength={50} />

                            <FlatList
                                style={{ height:cliente.length>0 ? 130 : 0,flex:1,left:0,position:'absolute',right:0,top:40,zIndex:1,
                                borderWidth: 1,borderColor:'#838892', backgroundColor: 'white', borderRadius:6,padding:0,elevation:4,
                                shadowColor: "black", shadowOpacity: 0.8,shadowOffset: {
                                height: 2, width: 1,}}}
                                data={cliente}
                                keyExtractor= {(item,index)=>item.id.toString()}
                                showsVerticalScrollIndicator={true}
                                renderItem={({ item, index }) => {
                                    if(flags.isLoadingFilterCliente){
                                      return <Loading loadingSize={40} color='orange'></Loading>
                                    }
                                    return (
                                      <View style={{flex:1}}>
                                      <Pressable style={{borderBottomWidth:1,borderBottomColor:'black',height:40,alignContent:'center',alignItems:'flex-start',justifyContent:'center',paddingHorizontal:8}}
                                      onPress={()=>{
                                          //console.log(item)
                                          clearFilterResults()

                                            const payload= relatorio;
                                            payload.omitFilter=true;

                                            if(item.razaoSocial=='Não há dados'){
                                              payload.filtroCliente ='';
                                              payload.filtroClienteId= 0;
                                            }else{
                                              payload.filtroCliente =item.razaoSocial;
                                              payload.filtroClienteId= item.id;
                                            }
                                            
                                            setRelatorio(payload)


                                      }} >
                                        <Text style={{margin:6,fontFamily:'Roboto-Regular',color:'#000000', fontSize:11,backgroundColor:'transparent',borderRadius:8, }}>{item.razaoSocial}</Text>
                                      </Pressable>
                                      </View>
                                    );
                                  }}

                            />


                         </>
                        }
                        
                          
                        <Spacer height={15}></Spacer>
                        {/* filtros fechas*/}
                        <View style={{flexDirection:'row'}}>
                            <FechaInput filtroFecha={relatorio.filtroFechaInicial} setFiltroFecha={setRelatorio} iniFini='ini' placeHolder='Fecha inicial'></FechaInput>
                            <View style={{width:30}}></View>
                            <FechaInput filtroFecha={relatorio.filtroFechaFinal} setFiltroFecha={setRelatorio} iniFini='fini' placeHolder='Fecha final'></FechaInput>
                        </View>
              </View>
}


