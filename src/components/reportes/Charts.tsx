import React, { useContext } from 'react';
import {  Text, View } from 'react-native';
import { useRelatorios } from '../../hooks/useRelatorios';
import { colores } from '../../theme/appTheme';
import { GeneralContext } from '../../state/GeneralProvider';
import { BarChart } from 'react-native-gifted-charts';
import { IndicadorSimple } from './IndicadorSimple';
import { IndicadorDoble } from './IndicadorDoble';
import { TituloGrafico } from './TituloGrafico';
import { useHome } from '../../hooks/useHome';
import { Loading } from '../Loading';


export const Charts = () => {


  let colorIcono = colores.primary;
  //invoke global state
  const { relatorio,graphData,graphDataParticipaciones,flags } = useContext( GeneralContext )
  // const {  floading,totalPareceres,maxGrafica } = useHome(); 
  // const { onChangeFiltroCliente } = useRelatorios(); 

  const labelTopGo = () =>{  return( <Text style={{width:70, textAlign:'right' , color:'#000000',right:2}}>{graphData.go}</Text> ) }
  const labelTopNoGo = () =>{  return( <Text style={{width:70, textAlign:'right' , color:'#000000',right:2}}>{graphData.noGo}</Text> ) }
  const labelEnAnalisis = () =>{  return( <Text style={{width:70, textAlign:'right' , color:'#000000',right:2}}>{graphData.analise}</Text> ) }

  const labelVencidas= () =>{  return( <Text style={{textAlign:'center', color:'#000000',left:10}}>{ graphDataParticipaciones.vencidas }</Text> ) }
  const labelPerdidas = () =>{  return( <Text style={{textAlign:'center' , color:'#000000',left:10}}>{ graphDataParticipaciones.perdidas }</Text> ) }
  const labelSuspensas = () =>{  return( <Text style={{textAlign:'center' , color:'#000000',left:10}}>{ graphDataParticipaciones.suspensas }</Text> ) }
  const labelFracasadas = () =>{  return( <Text style={{textAlign:'center' , color:'#000000',left:10}}>{ graphDataParticipaciones.fracassadas } </Text> ) }
  const labelAndamento = () =>{  return( <Text style={{textAlign:'center' , color:'#000000',left:10}}>{ graphDataParticipaciones.andamento }</Text> ) }


  const barDataParecer = [
    {value: graphData.go,label: 'GO',frontColor: '#83AE69',topLabelComponent:labelTopGo,labelTextStyle:{color:'#000000'} },
    {value: graphData.noGo,label: 'NO GO',frontColor: '#B85050',topLabelComponent:labelTopNoGo,labelTextStyle:{color:'#000000' }},
    {value: graphData.analise,label: 'EM ANÁLISE',frontColor: '#F9A61A',topLabelComponent:labelEnAnalisis , labelTextStyle: {color: '#000000'}},
   
    ];

const barDataParticipaciones = [
  {value: graphDataParticipaciones.vencidas,label: '',frontColor: '#28a745',topLabelComponent:labelVencidas },
  {value: graphDataParticipaciones.perdidas,label: '',frontColor: '#dc3545',topLabelComponent:labelPerdidas },
  {value: graphDataParticipaciones.suspensas,label: '',frontColor: '#fd7e14',topLabelComponent:labelSuspensas },
  {value: graphDataParticipaciones.fracassadas,label: '',frontColor: '#dc3545',topLabelComponent:labelFracasadas },
  {value: graphDataParticipaciones.andamento,label: '',frontColor: '#007bff',topLabelComponent:labelAndamento },
  ];

 if(flags.isLoadingRelatorio){
        return <View style={{  width:'90%',marginHorizontal:17, flex:1, justifyContent:'center',
                alignItems:'center', borderWidth: 0,backgroundColor:'transparent',padding:5,
               }}>
                  <Loading loadingSize={40} color='orange'  backgroundColor='transparent'></Loading>
              </View>
    }
 

    return    <View style={{  width:'90%',marginHorizontal:17, flexGrow:1, height: !relatorio.isFilterCollapsed ? '20%' : '68%', justifyContent:'center',
                    alignItems:'flex-start', borderWidth: 0,backgroundColor:'white', borderRadius:7,padding:5,elevation:6,
                    shadowColor: "#000000", shadowOpacity: 0.4,shadowOffset: { height: 1, width: 1}}}>   

                         <TituloGrafico></TituloGrafico>
                          
                          <BarChart barWidth={relatorio.isSelectorParecer ? 85 : 50}
                                    height={!relatorio.isFilterCollapsed ? 220 : 350}
                                    barBorderRadius={4}
                                    verticalLinesThickness={0}
                                    cappedBars={false}
                                    capThickness={3}
                                    xAxisColor={'#000000'}
                                    rulesColor={'#000000'}
                                    xAxisIndicesColor={'#000000'}
                                    yAxisIndicesColor={'#000000'}
                                    yAxisColor={'#000000'}
                                    yAxisTextStyle={{color:'#000000'}}
                                    capColor={'black'}
                                    spacing={1}
                                    initialSpacing={10}
                                    showFractionalValues={true}
                                    showLine={false}
                                    showXAxisIndices={true}                           
                                    showVerticalLines={true}
                                    showYAxisIndices={true}
                                    noOfSections={5}
                                    width={280}
                                    maxValue={0}
                                    data={relatorio.isSelectorParecer ? barDataParecer : barDataParticipaciones}
                                    isAnimated={true} />
                          
                            {/* indicadores de color */}
                            <View style={{marginBottom:0}}>
                                {relatorio.isSelectorParecer && <IndicadorSimple></IndicadorSimple>}

                                {!relatorio.isSelectorParecer && <IndicadorDoble></IndicadorDoble> }
                            </View>
                            {/* <Spacer height={5}></Spacer> */}
                </View>

      
}
