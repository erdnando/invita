import React, { useEffect, useState } from 'react';
import { Platform, Text, View } from 'react-native';
import { BarChart } from 'react-native-gifted-charts';
import { HeaderTitle } from '../HeaderTitle';
import { useHome } from '../../hooks/useHome';
import { GraphMotiveGoNoGo } from '../../models/response/GraphMotiveGoNoGo';
import { Spacer } from '../Spacer';


interface Props{
    height:number
  }

export const ResultadoParecer = ( ) => {

  const {  graphData,totalPareceres,maxGrafica } = useHome(); 


  const labelTopGo = () =>{  return( <Text style={{width:70, textAlign:'right',color:'black',right:2}}>{graphData.go}</Text> ) }
  const labelTopNoGo = () =>{  return( <Text style={{width:70, textAlign:'right',color:'black',right:2}}>{graphData.noGo}</Text> ) }
  const labelEnAnalisis = () =>{  return( <Text style={{width:70, textAlign:'right',color:'black',right:2}}>{graphData.analise}</Text> ) }
  

  const barData = [
      {value: graphData.go,label: 'GO',frontColor: '#83AE69',topLabelComponent:labelTopGo,labelTextStyle:{color:'#000000',fontSize:11} },
      {value: graphData.noGo,label: 'NO GO',frontColor: '#B85050',topLabelComponent:labelTopNoGo,labelTextStyle:{color:'#000000',fontSize:11} },
      {value: graphData.analise,label: 'EM ANÁLISE',frontColor: '#F9A61A',topLabelComponent:labelEnAnalisis,labelTextStyle:{color:'#000000',fontSize:11} },
     
      ];

    return  <View style={{flex:1,  flexDirection:'column',backgroundColor:'#BCC1CB', 
                        width:'100%',  alignItems:'center',}}>
                    <HeaderTitle label='Resultado do parecer' top={20} fontSize={17}></HeaderTitle>
                    <Spacer height={20}></Spacer>
                    <View style={{flex:1, marginBottom:8, flexDirection:'column', width:'90%', left:-1, justifyContent:'center', 
                        alignItems:'flex-start', borderWidth: 0,backgroundColor:'white', borderRadius:7,padding:5,elevation:6,
                        shadowColor: "#000000", shadowOpacity: 0.4,shadowOffset: {
                        height: 1,
                        width: 1}}}>         
                              <BarChart
                              initialSpacing={10}
                              yAxisTextStyle={{color:'#000000'}}
                              barWidth={80}
                              width={280}
                              spacing={5}
                              xAxisColor={'#000000'}
                              yAxisColor={'#000000'}
                              rulesColor={'#000000'}
                              xAxisIndicesColor={'#000000'}
                              yAxisIndicesColor={'#000000'}
                              barBorderRadius={4}
                              verticalLinesThickness={0}
                              cappedBars={false}
                              capThickness={3}
                              capColor={'black'}
                              showFractionalValues={true}
                              showLine={false}
                              showXAxisIndices={true}
                              showReferenceLine1={true}
                              showVerticalLines={true}
                              showYAxisIndices={true}
                              noOfSections={5}
                              maxValue={maxGrafica}
                              data={barData}
                              isAnimated/>
                    </View>
                    <View style={{height:90,alignContent:'flex-start',justifyContent:'flex-start',alignItems:'flex-start'}}>
                       <HeaderTitle label={`Total de Pareceres: ${totalPareceres}`} top={5} fontSize={17}></HeaderTitle>
                    </View>
                    
            </View>
}
