import React from 'react'

import { KeyboardAvoidingView, ScrollView, TouchableWithoutFeedback,Keyboard} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'

const KeyboardWrapper=({children}) =>{
    return (
        <SafeAreaView>
        <KeyboardAvoidingView behavior='position'>
            <ScrollView >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}   style={{ flex: 1 }}>
                    {children}
                </TouchableWithoutFeedback>
            </ScrollView>
        </KeyboardAvoidingView>
        </SafeAreaView>
    );
}

export default KeyboardWrapper;