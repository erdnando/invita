import React, { useContext } from 'react';
import { FlatList, Image, Modal, Text, View } from 'react-native';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { GeneralContext } from '../../state/GeneralProvider';
import { Spacer } from '../Spacer';
import { Loading } from '../Loading';
import { IconoActualizacion } from '../ultimasActualizaciones/IconoActualizacion';
import { ResponseEstatus } from '../../models/response/ResponseEstatus';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import CustomIcon from '../../theme/CustomIcon';

export const ListEstatus = ( ) => {

 const { flags,estatus } = useContext( GeneralContext );


const renderUpdateItem = (updateItem:ResponseEstatus) =>{
    return (
        <View style={{flexDirection:'row',justifyContent:'flex-end',backgroundColor:'transparent',height:33}}>
            <View style={{width:'18%',backgroundColor:'transparent'}}>
                {/* label hoy/ayer */}
                <View  style={{left:-23,top:12,width:60,elevation:0,justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                   {/* <CustomIcon name='ic_baseline-arrow-back' size={23} color='black' ></CustomIcon> */}
                  { updateItem.id==1 ? <Image style={{justifyContent:'center', alignItems:'center', width:30,height:30}} source={require('../../assets/flechaMorada.png')} ></Image> :
                                       <Image style={{justifyContent:'center', alignItems:'center', width:30,height:30}} source={require('../../assets/flechaGris.png')} ></Image>}
                </View>
             
               {/* linea */}
                {updateItem.id==1 ? <View></View> : <View style={{backgroundColor:'#a9b5ba',left:6, width:3,opacity:0.5,top:0, height:11,position:'absolute',}}></View>}
            </View>

           {/* alerta88 */}
            <View style={{height:40, flexDirection:'row', width:'77%', borderWidth: 0,backgroundColor:'transparent',top:8 }}>

                <View style={{ flexDirection:'column', width:'82%',left:-10,top:6, justifyContent:'flex-start',  alignItems:'flex-start'}}>
                   {updateItem.estatus==1 ?  <Text style={{fontFamily:'Roboto-Bold', fontSize:14, color: '#565353'}}>{updateItem.Descripcion}</Text> : 
                   <Text style={{fontFamily:'Roboto-Bold', fontSize:14, color: '#a9b5ba'}}>{updateItem.Descripcion}</Text>}
                </View>

                
            </View>
            
        </View>
    )
}

    const renderSeparator = () =>{
        return (
            <View style={{width:3,opacity:0, justifyContent:'flex-end',left:20, backgroundColor:'#838892'}}>
                <Spacer height={10} ></Spacer>
            </View>
        )
    }


  if(estatus.length>0)
   { return (
      <View style={{flexGrow:1,backgroundColor:'white', justifyContent:'center',alignItems:'flex-start',alignContent:'flex-end', 
      top:10, width:'100%',height:450 }}>
        
        <FlatList data={estatus} 
        scrollEnabled={true}
        renderItem={ ({ item,index }) =>renderUpdateItem(item) } 
        keyExtractor={(item,index) => item.id.toString() + index} 
        ItemSeparatorComponent={ () => renderSeparator()}
        />
       
    </View>
    )}
    else{
        return (
            <View style={{ flexGrow:0,marginHorizontal:20,height:250, width:'90%',padding:20, justifyContent:'center',
                           alignContent:'center',backgroundColor:'white',top:10, alignItems:'center',
                           shadowColor: "#000000", shadowOpacity: 0.4,shadowOffset: {
                              height: 1, width: 1
                           }}}>

              <Image style={{justifyContent:'center', alignItems:'center', width:100,height:100}} source={require('../../assets/vertical-logo.png')} ></Image>
              <Spacer height={40}></Spacer>
              <Text style={{fontFamily:'Roboto-Regular', fontSize:22,marginTop:8,width:'100%',textAlign:'center', color:'#838892'}}>Nenhuma atualização</Text>
           
        </View>    
        )
    }
}
