import React, { useContext } from 'react';
import { ImageBackground, View } from 'react-native';
import { InputSearch } from '../InputSearch';
import { InputSearchEvento } from '../InputSearchEvento';
import { LabelTitulo } from '../LabelTitulo';
import { Spacer } from '../Spacer';

interface Props{
    searchTitulo:string
}

export const Encabezado = ( params:Props ) => {

 
    return   <ImageBackground style={{backgroundColor:'grey', height:300,alignContent:'center',alignItems:'center', justifyContent:'flex-end' }}  
                              resizeMode='cover' source={require('../../assets/home3.png')}>
                
                <View style={{width:'100%',left:-15}}>
                   <LabelTitulo fontSizeTitulo={22} fontSizeSubtitulo={16} labelTitulo={'¡Hola, bienvenidos!'} separacion={0} alineacion='left'
                            labelSubTitulo={'Dejanos ser parte de tu super evento'} colorTitulo={'white'} colorSubTitulo={'white'} ></LabelTitulo>
                </View>

                {/* <InputSearchEvento></InputSearchEvento> */}
                <View style={{width:'100%', marginLeft:14,marginRight:10, marginTop:8}}>
                   <InputSearchEvento></InputSearchEvento>
                </View>

                <Spacer height={10}></Spacer>
              
           </ImageBackground>
          
}
