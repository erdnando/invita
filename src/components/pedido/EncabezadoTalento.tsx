import React, { useContext } from 'react';
import { Image, ImageBackground, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useSearch } from '../../hooks/useSearch';
import { GeneralContext } from '../../state/GeneralProvider';
import { InputSearch } from '../InputSearch';
import { InputSearchEvento } from '../InputSearchEvento';
import { LabelTalento } from '../LabelTalento';
import { LabelTitulo } from '../LabelTitulo';
import { OpcionHeader } from '../OpcionHeader';
import { Spacer } from '../Spacer';

// interface Props{
//     searchTitulo:string
// }

export const EncabezadoTalento = () => {

    const { top } = useSafeAreaInsets();
    const { flags,setFlags,talentoSelected,ids,setIds } = useContext(GeneralContext);
    const { onChangeSearch } = useSearch();
 
    return   <ImageBackground style={{backgroundColor:'grey', height:300,alignContent:'center',alignItems:'center', justifyContent:'flex-end' }}  
                              resizeMode='cover' source={talentoSelected.imagen}>

                        <View style={{flex:0,position:'absolute',  alignItems:'flex-start',width:'100%',top:top,
                                          flexDirection:'column',backgroundColor:'transparent', height:40}}>

                                    <View style={{flex:0,flexDirection:'row',width:'100%',justifyContent:'space-between'}}>
                                        <OpcionHeader iconName='ic_baseline-arrow-back' color='white'  // back option 
                                            onPress={() =>{ 
                                                    console.log('going back..xx')
                                                    const payload= flags;
                                                    payload.modalTalentoVisible= false;
                                                    setFlags(payload);

                                                    const payload1= ids;
                                                    payload1.busquedaTalento= talentoSelected.talento.trim();
                                                    setIds(payload1);
                                                    onChangeSearch(talentoSelected.talento.trim())
                                            }} /> 

                     
                                         <View style={{flexDirection:'row',justifyContent:'center',alignContent:'center',alignItems:'center',marginTop:25,marginRight:20 }}>
                                                        <Image style={{ alignItems: 'center', width: 20,height:20,marginTop:2,marginRight:2 }}
                                                                source={{uri:'https://icon-library.com/images/yellow-star-icon/yellow-star-icon-19.jpg' }}></Image>
                                                        <Text style={{fontFamily:'Roboto-Regular', fontSize:18,fontWeight:'600',top:3,marginLeft:4, textAlign:'left',color:'white',}}>{talentoSelected.valoracion+ '.0'}</Text>
                                         </View>



                                    {/*  */}
                                    </View>
                                    

                                    <View style={{width:'100%',left:-5,marginTop:25}}>
                                    <LabelTitulo fontSizeTitulo={24} fontSizeSubtitulo={16} labelTitulo={talentoSelected.talento} separacion={0} alineacion='left'
                                                labelSubTitulo={''} colorTitulo={'white'} colorSubTitulo={'white'} ></LabelTitulo>
                                    </View>
                        
                        </View>
                
                        {/*  */}

                        {/* <InputSearchEvento></InputSearchEvento> */}
                        {/* <View style={{width:'100%', marginLeft:14,marginRight:10, marginTop:8}}>
                        <InputSearchEvento></InputSearchEvento>
                        </View> */}

                        <Spacer height={10}></Spacer>
              
           </ImageBackground>
          
}
