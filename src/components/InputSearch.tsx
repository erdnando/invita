import React, { useContext } from 'react';
import { Text, TextInput, TouchableOpacity, View, Platform ,Keyboard} from 'react-native';
import { GeneralContext } from '../state/GeneralProvider';
import CustomIcon from '../theme/CustomIcon';
import { colores } from '../theme/appTheme';
import { useSearch } from '../hooks/useSearch';
import { useNavigation } from '@react-navigation/native';


interface Props{
  label:string,
  iconRight:string,
  color:string
}

export const InputSearch = ( { label, iconRight,color}: Props ) => {

  let colorIcono = colores.primary;

    //invoke global state
    const { ids,flags,setFlags } = useContext( GeneralContext )
    const { onChangeSearch,getResultadoBusqueda } = useSearch();
    const navigation = useNavigation();
    
    return (
      <View style={{flexDirection: 'row',justifyContent: 'center', }}>
        
 
            <TextInput style={{flex:1,
                  fontFamily:'Roboto-Regular',
                  height: 40,
                  margin: 12,
                  left:4,
                  paddingLeft:31, borderWidth:Platform.OS=='android' ? 3 : 0,borderColor:Platform.OS=='android' ? '#E2E5EA' : 'transparent',
                  borderRadius: 9, padding:5,elevation:0,backgroundColor : "white",
                  shadowColor: "black", shadowOpacity: 0.4,shadowOffset: {
                   height: 1,
                   width: 1
                 },
                 color:'#000000'
              }}
              onChangeText={ onChangeSearch }
              placeholder={label}
              placeholderTextColor={'#757575'}
              keyboardType='numeric'
              onSubmitEditing={()=>{
                console.log('buscando...');
              }}
              autoCapitalize='none'
              autoCorrect = {false}
              maxLength={27}
              value={ids.busquedaTalento}
          />

         <View style={{position:'absolute', left:22,top:22}}>
             <CustomIcon   name={iconRight} size={24} color= {ids.codigoBusqueda===''?'black':colorIcono}  ></CustomIcon>
         </View>


          <TouchableOpacity activeOpacity={0.9}  
                  style={{ right:20, top:11,backgroundColor:color,height:42,flex:0,shadowColor: "black",
                           borderRadius:9, shadowOpacity: 0.4,shadowOffset: {
                           height: 1, width: 1 },}} onPress={() =>{ 
               
                getResultadoBusqueda()
              
                navigation.navigate({ name: 'BusquedaScreen' } as never);
            }}>
              
             <Text style={{left:18,top:11,width:80,color:'white',fontWeight:'bold'}}>
              Buscar
                       
              </Text>  
          </TouchableOpacity>
     
         
     
      </View>
    )
}
