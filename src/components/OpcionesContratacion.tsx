import React, { useContext } from 'react';
import { View } from 'react-native';
import { GeneralContext } from '../state/GeneralProvider';
import { LabelTitulo } from './LabelTitulo';
import { Select } from './Select';




export const OpcionesContratacion = (  ) => {

  const { flags,setFlags,talentoSelected,ids,setIds,setTalentoSelected } = useContext(GeneralContext);

    return   <View style={{flex:1, flexDirection:'row',marginLeft:30,marginRight:10, }}>
                  <View style={{flex:1,}}> 
                        <LabelTitulo fontWeight='500' marginBottom={-10} marginHorizontal={1} fontSizeTitulo={15} fontSizeSubtitulo={14} labelTitulo={'Integrantes'} separacion={0} alineacion='left'
                        labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                        <Select placeholder='---------' campo={talentoSelected.integrantesSeleccionado} items={talentoSelected.contratacion.integrantes}
                              onValueChange={function (value: string, index: number): void {

                                    const payload = talentoSelected;
                                    payload.integrantesSeleccionado=value;
                                    payload.integrantesFinal=value.toString().trim().split('|')[0];
                                    payload.costoFinal=value.toString().trim().split('|')[1];

                                    payload.costoFinalmultiplicado= value.toString().trim().split('|')[1]
                                    payload.duracion="1";
                                    setTalentoSelected(payload);

                                    console.log('integrantes::')
                                    console.log(talentoSelected.integrantesFinal)
                                    console.log('costoFinal::')
                                    console.log(talentoSelected.costoFinal)
                              }} 
                              /> 
                  </View>

                  <View style={{flex:1}}>
                        <LabelTitulo fontWeight='500'  marginBottom={-10} marginHorizontal={1} fontSizeTitulo={15} fontSizeSubtitulo={14} labelTitulo={'Duracion/hr'} separacion={0} alineacion='left'
                        labelSubTitulo={''} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTitulo>
                        <Select placeholder='---------' campo={talentoSelected.duracion} items={talentoSelected.contratacion.duracion}
                              onValueChange={function (value: any, index: number): void {

                                    
                                    const payload = talentoSelected;
                                    payload.duracion=value;
                                    setTalentoSelected(payload);

                                    console.log('duracion::')
                                    console.log(talentoSelected.duracion)

                                    const payload1 = talentoSelected;
                                   console.log('multiplicando '+parseFloat(payload1.costoFinal) + ' X ' + parseFloat(payload1.duracion) );

                                   let param1 = parseFloat(payload1.costoFinal);
                                   let param2= isNaN(parseFloat(payload1.duracion.toString() ))  ? 1 : parseFloat(payload1.duracion.toString() );

                                   payload1.costoFinalmultiplicado= (param1*param2  ).toString();
                                    setTalentoSelected(payload1);
                                    
                              }} 
                              /> 
                  </View>
             </View>
}
