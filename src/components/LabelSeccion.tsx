import React from 'react';
import { Text, View, TextStyle } from 'react-native';
import { Spacer } from './Spacer';

interface Props{
  fontSizeTitulo:number,
  labelTitulo:string,
  colorTitulo:string,
  alineacion?:TextStyle['textAlign'],
}

export const LabelSeccion = ( { fontSizeTitulo,labelTitulo,colorTitulo,alineacion='center' }: Props ) => {

     return <View style={{width:200,}}>   
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSizeTitulo,fontWeight:'700', textAlign:alineacion, marginBottom:3, color:colorTitulo}}>{labelTitulo}</Text>
            </View>
          
}
