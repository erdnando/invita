import React from 'react';
import { Text, View, TextStyle, Image } from 'react-native';
import { Spacer } from './Spacer';

interface Props{
  fontSizeTitulo:number,
  fontSizeSubtitulo:number,
  talento:string,
  categoria:string,
  calificacion:number,
  colorTitulo:string,
  colorSubTitulo:string,
  separacion?:number,
  alineacion?:TextStyle['textAlign'],
}

export const LabelTalento = ( { fontSizeTitulo,fontSizeSubtitulo, talento,categoria,calificacion,colorTitulo,colorSubTitulo,separacion=8,alineacion='center' }: Props ) => {



     return <View style={{ width:'90%',marginHorizontal:0}}>   
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSizeTitulo,fontWeight:'500', textAlign:alineacion, marginBottom:3, color:colorTitulo}}>{talento}</Text>
               <Spacer height={separacion}></Spacer>
               <View style={{flexDirection:'row' }}>
                  <Image style={{ alignItems: 'center', width: 12,height:10,marginTop:2,marginRight:2 }}
                        source={{uri:'https://icon-library.com/images/yellow-star-icon/yellow-star-icon-19.jpg' }}></Image>
                  <Text style={{fontFamily:'Roboto-Regular', fontSize:13,fontWeight:'600', textAlign:alineacion,color:colorSubTitulo,}}>{calificacion}</Text>
                  <View style={{width:10}}></View>
                  <Text style={{fontFamily:'Roboto-Regular', fontSize:12,fontWeight:'400', textAlign:alineacion,color:colorSubTitulo,}}>{categoria}</Text>
               </View>
            
            </View>
          
}
