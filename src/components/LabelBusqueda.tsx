import React from 'react';
import { Text, View, TextStyle } from 'react-native';
import { Spacer } from './Spacer';

interface Props{
  fontSize:number,
  label1:string,
  label2:string,
  label3:string,
  label4:string,
  color:string,
  marginHorizontal?:number,
}

export const LabelBusqueda = ( { color,fontSize,label1,label2,label3,label4,marginHorizontal=30 }: Props ) => {



     return <View style={{ flexDirection:'row',flexWrap:'wrap',width:'90%',marginHorizontal:marginHorizontal}}>   
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSize,fontWeight:'400',  color:color}}>{label1}</Text>
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSize,fontWeight:'700', color:color, marginHorizontal:3}}>{label2}</Text>
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSize,fontWeight:'400',color:color,marginRight:3}}>{label3}</Text>
               <Text style={{fontFamily:'Roboto-Regular', fontSize:fontSize,fontWeight:'700',color:color,}}>{label4}</Text>
            </View>
          
}
