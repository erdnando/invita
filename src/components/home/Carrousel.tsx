import React, { useContext } from 'react';
import { View, Animated, StyleSheet, Platform, Image, TouchableOpacity, } from 'react-native';
import { CategoriasResponse } from '../../models/response/CategoriasResponse';
import { GeneralContext } from '../../state/GeneralProvider';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';
import { useSearch } from '../../hooks/useSearch';


interface Props{
    param1:string,
  }

export const Carrousel = ( ) => {

    const {flags,categorias,setCategorias,setFlags  } = useContext( GeneralContext );
    const navigation = useNavigation();
    const { onChangeSearch } = useSearch();

    function renderUpdateItem(updateItem: CategoriasResponse) {

        return (
            <TouchableOpacity activeOpacity={0.2}
                style={{
                    backgroundColor: 'transparent', flex: 1, shadowColor: "black",
                    borderRadius: 9, shadowOpacity: 0.4, shadowOffset: {
                        height: 1, width: 1
                    },
                }} onPress={() => {
//                    navigation.navigate({ name: 'CategoriaScreen' } as never);
                      navigation.navigate({ name: 'BusquedaScreen' } as never);
                      onChangeSearch(updateItem.descripcion)
                } }>

                <Image style={{ justifyContent: 'center', backgroundColor: 'transparent', alignItems: 'center', width: 68, height: 85 }}
                    source={updateItem.background}></Image>
            </TouchableOpacity>
        );

    }

    const renderSeparator = () =>{
        return (
            <View style={{width:15, justifyContent:'flex-start', backgroundColor:'transparent'}}>
               
            </View>
        )
        }

    return (
    <View style={{}}>
        
          <KeyboardAwareFlatList data={categorias} 
          horizontal={true}
          scrollEnabled={true}
          renderItem={ ({ item,index }) =>renderUpdateItem(item) } 
          keyExtractor={(item,index) => item.id + index} 
          ItemSeparatorComponent={ () => renderSeparator()}
      />
    </View>
    )
       
}


const styles = StyleSheet.create({
    card:{
        flexGrow:1,backgroundColor:'#BCC1CB', justifyContent:'center',alignItems:'flex-start',
        alignContent:'flex-end', top:52, width:'100%',
    },
    checkbox:{
        backgroundColor : Platform.OS==='ios' ? 'white' : 'transparent',
        transform : Platform.OS==='ios' ? [{ scaleX: 1 }, { scaleY: 1 }] : [{ scaleX: 1.7 }, { scaleY: 1.7 }]
    }
});