import React, { useContext } from 'react';
import { View, Animated, StyleSheet, Platform, Image, TouchableOpacity, ActivityIndicator, } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';
import { TalentoBusqueda } from '../TalentoBusqueda';
import { TalentoResponse } from '../../models/response/TalentoResponse';
import { useImages } from '../../hooks/useImages';
import { Resenia } from '../../models/Resenia';
import { ReseniaCard } from '../ReseniaCard';


export const CarrouselResenias = () => {

    const { talentosBusqueda,flags,setFlags,talentoSelected,setTalentoSelected  } = useContext( GeneralContext );
    const navigation = useNavigation();
    const images = useImages();

    function renderUpdateItem(updateItem: Resenia) {

        return  <ReseniaCard  fontSizeTitulo={12} cliente={updateItem.cliente} fecha={updateItem.fecha} resenia={updateItem.resenia} separacion={0} alineacion='left'
        calificacion={updateItem.calificacion} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></ReseniaCard>
    }

    const renderSeparator = () =>{
        return (
            <View style={{width:15, justifyContent:'flex-start', backgroundColor:'transparent',height:15}}></View>
        )
        }

    return (
    <View style={{}}>
        
          <KeyboardAwareFlatList data={talentoSelected.resenias} 
          horizontal={false}
          scrollEnabled={true}
          renderItem={ ({ item,index }) =>renderUpdateItem(item) } 
          keyExtractor={(item,index) => item + index} 
          ItemSeparatorComponent={ () => renderSeparator()}
      />
    </View>
    )
       
}


const styles = StyleSheet.create({
    card:{
        flexGrow:1,backgroundColor:'#BCC1CB', justifyContent:'center',alignItems:'flex-start',
        alignContent:'flex-end', top:52, width:'100%',
    },
    checkbox:{
        backgroundColor : Platform.OS==='ios' ? 'white' : 'transparent',
        transform : Platform.OS==='ios' ? [{ scaleX: 1 }, { scaleY: 1 }] : [{ scaleX: 1.7 }, { scaleY: 1.7 }]
    }
});