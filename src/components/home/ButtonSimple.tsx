import { DrawerContentComponentProps } from '@react-navigation/drawer';
import React, { useContext } from 'react';
import { Text, TouchableOpacity, View, TextStyle } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import CustomIcon from '../../theme/CustomIcon';


interface Props{
    backgroundColor:string,
  color:string,
  label:string,
  textDecorationLine:TextStyle["textDecorationLine"],
  disabled?:boolean,
  onPress: () => void;
}

export const ButtonSimple = ( { backgroundColor, color, label,textDecorationLine,disabled=false,onPress }: Props ) => {

 
    return  <View style={{ flexDirection:'row', justifyContent:'flex-start',alignItems:'center', top:0,}}> 

              <TouchableOpacity disabled={disabled} style={{ padding:12,margin:2, justifyContent:'center',alignItems:'center', backgroundColor:disabled ? 'grey' :backgroundColor,width:160,borderRadius:6   }} onPress={onPress} >
                  <Text style={{color:color, textDecorationLine:textDecorationLine}}>
                      {label}
                  </Text>
                  
              </TouchableOpacity>

            
          </View>
          
}
