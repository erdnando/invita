import React, { useContext } from 'react';
import { ImageBackground, View } from 'react-native';
import { InputSearch } from '../InputSearch';
import { LabelTitulo } from '../LabelTitulo';
import { Spacer } from '../Spacer';


interface Props{
    searchTitulo:string
}

export const Encabezado = ( params:Props ) => {

 
    return   <ImageBackground style={{backgroundColor:'grey', height:300,alignContent:'center',alignItems:'center', justifyContent:'flex-end' }}  
                              resizeMode='cover' source={require('../../assets/home3.png')}>
                
                <View style={{width:'100%',left:-15}}>
                   <LabelTitulo fontSizeTitulo={22} fontSizeSubtitulo={16} labelTitulo={'¡Hola, bienvenidos!'} separacion={0} alineacion='left'
                            labelSubTitulo={'Dejanos ser parte de tu super evento'} colorTitulo={'white'} colorSubTitulo={'white'} ></LabelTitulo>
                </View>

                <InputSearch label={params.searchTitulo } iconRight={'ic_baseline-search'} color={'#110331'} ></InputSearch>

                <Spacer height={10}></Spacer>
              
           </ImageBackground>
          
}
