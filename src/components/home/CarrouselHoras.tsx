import React, { useContext } from 'react';
import { View, Animated, StyleSheet, Platform, Image, TouchableOpacity, Text, } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';
import { useSearch } from '../../hooks/useSearch';


interface Props{
    param1:string,
  }

export const CarrouselHoras = ( ) => {

    const {flags,talentoSelected,setTalentoSelected  } = useContext( GeneralContext );
    const navigation = useNavigation();
    const { onChangeSearch } = useSearch();

    function renderUpdateItem(updateItem: string) {
        return (
            <TouchableOpacity activeOpacity={0.2}
                style={{alignItems:'center',justifyContent:'center',
                    backgroundColor:  (updateItem ==  talentoSelected.horarioSeleccionado ? '#E42381':'#DCDCDD'), flex: 1, shadowColor: "black",width:70,height:45,
                    borderRadius: 12, shadowOpacity: 0.4, shadowOffset: {
                        height: 1, width: 1
                    },
                }} onPress={() => {
                    const payload= talentoSelected;
                    payload.horarioSeleccionado= updateItem;
                    setTalentoSelected(payload);
                    console.log('horario seleccionado:' + talentoSelected.horarioSeleccionado)
                } }>

            
                    <Text style={{ fontSize:16, color:(updateItem ==  talentoSelected.horarioSeleccionado ? 'white':'black'),   
                                 fontWeight :(updateItem ==  talentoSelected.horarioSeleccionado ? 'bold':'400')
                                 }} >{updateItem} </Text>
            </TouchableOpacity>
        );

    }

    const renderSeparator = () =>{
        return (
            <View style={{width:15, justifyContent:'flex-start', backgroundColor:'transparent'}}>
               
            </View>
        )
        }

    return (
    <View style={{}}>
        
          <KeyboardAwareFlatList data={talentoSelected.contratacion.horariosDisponibles} 
          horizontal={true}
          scrollEnabled={true}
          renderItem={ ({ item,index }) =>renderUpdateItem(item) } 
          keyExtractor={(item,index) => item} 
          ItemSeparatorComponent={ () => renderSeparator()}
      />
    </View>
    )
       
}


const styles = StyleSheet.create({
    card:{
        flexGrow:1,backgroundColor:'#BCC1CB', justifyContent:'center',alignItems:'flex-start',
        alignContent:'flex-end', top:52, width:'100%',
    },
    checkbox:{
        backgroundColor : Platform.OS==='ios' ? 'white' : 'transparent',
        transform : Platform.OS==='ios' ? [{ scaleX: 1 }, { scaleY: 1 }] : [{ scaleX: 1.7 }, { scaleY: 1.7 }]
    }
});