import React, { useContext } from 'react';
import { View, Animated, StyleSheet, Platform, Image, TouchableOpacity, ActivityIndicator, } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';
import { LabelTalento } from '../LabelTalento';
import { TalentoResponse } from '../../models/response/TalentoResponse';
import { Spacer } from '../Spacer';
import { useImages } from '../../hooks/useImages';
import { LoremPicsum } from 'react-lorem-picsum';


interface Props{
    param1:string,
  }

export const CarrouselTalento = ( ) => {

    const { talentosMasPopulares,flags,setFlags,talentoSelected,setTalentoSelected  } = useContext( GeneralContext );
    const navigation = useNavigation();
    const images = useImages();

    function renderUpdateItem(updateItem: TalentoResponse) {

     //  

        return (
            <TouchableOpacity activeOpacity={0.2}
                style={{ backgroundColor: 'transparent', flex: 1, shadowColor: "black",
                    borderRadius: 9, shadowOpacity: 0.4, shadowOffset: {
                        height: 1, width: 1
                    },
                }} onPress={() => {
                    
                    navigation.navigate({ name: 'BusquedaScreen' } as never);
                    const payload= flags;
                    payload.modalTalentoVisible= true;
                    setFlags(payload);

                    //set selected talento
                    const payload1= talentoSelected;
                    payload1.categoria =updateItem.categoria;
                    //payload1.costo =updateItem.costo;
                    payload1.costoNivel =updateItem.costoNivel;
                    payload1.descripcion =updateItem.descripcion;
                    payload1.fechaAlta =updateItem.fechaAlta;
                    payload1.id =updateItem.id;
                    payload1.imagen =updateItem.imagen;
                    payload1.popularidad =updateItem.popularidad;
                    payload1.talento =updateItem.talento;
                    payload1.valoracion =updateItem.valoracion;

                    payload1.contratacion= updateItem.contratacion;
                    
                    payload1.resenias=updateItem.resenias;
                    payload1.integrantesSeleccionado=updateItem.integrantesSeleccionado;
                    payload1.duracion=updateItem.duracion;
                    payload1.costoFinal=updateItem.costoFinal;
                    payload1.costoFinalmultiplicado="0";

                    
                    setTalentoSelected(payload1);


                } }>

                <Image style={{ alignItems: 'center', width: 160,height:95,borderRadius:8 }}
                   source={{uri:'https://picsum.photos/160/100/?random&t=' + new Date().getTime() }}></Image>

                    <Spacer height={8}></Spacer>
                
                <LabelTalento fontSizeTitulo={12} fontSizeSubtitulo={14} talento={updateItem.talento} separacion={0} alineacion='left'
                                             categoria={updateItem.categoria} calificacion={updateItem.valoracion} colorTitulo={'#110331'} colorSubTitulo={'#110331'} ></LabelTalento>

            </TouchableOpacity>
        );

    }

    const renderSeparator = () =>{
        return (
            <View style={{width:15, justifyContent:'flex-start', backgroundColor:'transparent'}}>
               
            </View>
        )
        }

    return (
    <View style={{}}>
        
          <KeyboardAwareFlatList data={talentosMasPopulares} 
          horizontal={true}
          scrollEnabled={true}
          renderItem={ ({ item,index }) =>renderUpdateItem(item) } 
          keyExtractor={(item,index) => item.id + index} 
          ItemSeparatorComponent={ () => renderSeparator()}
      />
    </View>
    )
       
}


const styles = StyleSheet.create({
    card:{
        flexGrow:1,backgroundColor:'#BCC1CB', justifyContent:'center',alignItems:'flex-start',
        alignContent:'flex-end', top:52, width:'100%',
    },
    checkbox:{
        backgroundColor : Platform.OS==='ios' ? 'white' : 'transparent',
        transform : Platform.OS==='ios' ? [{ scaleX: 1 }, { scaleY: 1 }] : [{ scaleX: 1.7 }, { scaleY: 1.7 }]
    }
});