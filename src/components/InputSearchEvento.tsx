import React, { useContext } from 'react';
import { Text, TextInput, TouchableOpacity, View, Platform ,Keyboard} from 'react-native';
import { GeneralContext } from '../state/GeneralProvider';
import CustomIcon from '../theme/CustomIcon';
import { colores } from '../theme/appTheme';
import { useSearch } from '../hooks/useSearch';
import { useNavigation } from '@react-navigation/native';


interface Props{
  label:string,
  iconRight:string,
  color:string
}

export const InputSearchEvento = (  ) => {

  let colorIcono = colores.primary;

    //invoke global state
    const { ids,flags,setFlags } = useContext( GeneralContext )
    const { onChangeSearchCodigo,getResultadoEvento } = useSearch();
    const navigation = useNavigation();
    
    return (
      <View style={{flexDirection: 'row',justifyContent: 'center',marginTop:-10 }}>
        
 
            <TextInput style={{flex:1,
                  fontFamily:'Roboto-Regular',
                  height: 40,
                  margin: 12,
                  left:4,
                  paddingLeft:12, borderWidth:Platform.OS=='android' ? 3 : 0,borderColor:Platform.OS=='android' ? '#E2E5EA' : 'transparent',
                  borderRadius: 9, padding:5,elevation:0,backgroundColor : "white",
                  shadowColor: "black", shadowOpacity: 0.4,shadowOffset: {
                   height: 1,
                   width: 1
                 },
                 color:'#000000'
              }}
              onChangeText={ onChangeSearchCodigo }
              placeholder={'Ingresa tu codigo'}
              placeholderTextColor={'#757575'}
              keyboardType='numeric'
              onSubmitEditing={()=>{
                console.log('buscando evento...');
              }}
              autoCapitalize='none'
              autoCorrect = {false}
              maxLength={27}
              value={ids.codigoEvento}
          />


          <TouchableOpacity activeOpacity={0.9}  
                  style={{ right:20, top:11,backgroundColor:'#110331',height:42,flex:0,shadowColor: "black",
                           borderRadius:9, shadowOpacity: 0.4,shadowOffset: {
                           height: 1, width: 1 },}} onPress={() =>{ 
               
                getResultadoEvento()
              
                navigation.navigate({ name: 'PedidoScreen' } as never);
            }}>
              
             <Text style={{left:18,top:11,width:80,color:'white',fontWeight:'bold'}}>
              Buscar
              </Text>  
          </TouchableOpacity>
     
         
     
      </View>
    )
}
