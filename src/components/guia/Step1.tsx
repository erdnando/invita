import React, { useContext } from 'react';
import { Image, View, Dimensions } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { ButtonSimple } from '../home/ButtonSimple';
import { LabelTitulo } from '../LabelTitulo';
import { Spacer } from '../Spacer';



export const Step1 = () => {

  const windowWidth = Dimensions.get('window').width;
  const { flags,setFlags } = useContext( GeneralContext )
   
   return <View style={{ flex:1,backgroundColor:'white',paddingTop:0, paddingHorizontal:8, justifyContent:'space-between',alignContent:'center',alignItems:'center' }}>
        
        <Spacer height={130}></Spacer>
        <View><Image style={{width:270,height:270}} source={require('../../assets/bienvenida.png')} ></Image></View>
       
            <LabelTitulo fontSizeTitulo={22} fontSizeSubtitulo={17} labelTitulo={'¡Hola, bienvenidos!'} 
                         labelSubTitulo={'Nosotros nos encargaremos de llevar los mejores artistas a tu evento'} colorTitulo={'black'} colorSubTitulo={'black'} ></LabelTitulo>
           

            <View style={{alignItems:'center'}}>
                <ButtonSimple  backgroundColor='#110331' color='white' label={'Siguiente'} textDecorationLine={'none'} onPress={ ()=> {
                      console.log('siguiente 02 ')
                      const payload= flags;
                      payload.guiaStep1 = false;
                      payload.guiaStep2 = true;
                      payload.guiaStep3 = false;
                      payload.guiaStep4 = false;
                      setFlags(payload);

                } }></ButtonSimple>
                <Spacer height={10}></Spacer>

                <View style={{backgroundColor:'white', width:windowWidth,alignItems:'flex-end', right:-20}}>
                  <ButtonSimple  backgroundColor='transparent' color='black' label={'Omitir'} textDecorationLine={'underline'} onPress={ ()=> {
                        console.log('omitir')
                          const payload= flags;
                          payload.modalGuia01Visible = false;
                          setFlags(payload);
                  } }></ButtonSimple>
              </View>

              <Spacer height={50}></Spacer>
            </View>

        </View> 

}
