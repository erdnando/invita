import React, { useContext } from 'react';
import { Dimensions, Image, Text, View } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { gstyles } from '../../theme/appTheme';
import { ButtonSimple } from '../home/ButtonSimple';
import { LabelTitulo } from '../LabelTitulo';
import { Spacer } from '../Spacer';



export const Step3 = () => {

  const windowWidth = Dimensions.get('window').width;
  const { flags,setFlags } = useContext( GeneralContext )
   
   return <View style={{ flex:1,backgroundColor:'white',paddingTop:0, paddingHorizontal:8, justifyContent:'space-between',alignContent:'center',alignItems:'center' }}>
        
        <Spacer height={130}></Spacer>
        <View  ><Image style={{width:270,height:270}} source={require('../../assets/reserva.png')} ></Image></View>
       
            <LabelTitulo fontSizeTitulo={22} fontSizeSubtitulo={17} labelTitulo={'Concreta tu reserva'} 
                         labelSubTitulo={'Comprueba la disponibilidad de tu talento, reserva y !listo¡'} colorTitulo={'black'} colorSubTitulo={'black'} ></LabelTitulo>
           

            <View style={{alignItems:'center'}}>
                <ButtonSimple  backgroundColor='#110331' color='white' label={'Siguiente'} textDecorationLine={'none'} onPress={ ()=> {
                      console.log('siguiente 04 ')
                      const payload= flags;
                      payload.guiaStep1 = false;
                      payload.guiaStep2 = false;
                      payload.guiaStep3 = false;
                      payload.guiaStep4 = true;
                      setFlags(payload);

                } }></ButtonSimple>
                <Spacer height={10}></Spacer>

                <View style={{backgroundColor:'white', width:windowWidth,alignItems:'flex-end', right:-20}}>
                  <ButtonSimple  backgroundColor='transparent' color='black' label={'Omitir'} textDecorationLine={'underline'} onPress={ ()=> {
                        console.log('omitir')
                          const payload= flags;
                          payload.modalGuia01Visible = false;
                          setFlags(payload);
                  } }></ButtonSimple>
              </View>

              <Spacer height={50}></Spacer>
            </View>

        </View> 

}
