import React, { useContext } from 'react';
import { Dimensions, Image, Text, View } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { gstyles } from '../../theme/appTheme';
import { ButtonSimple } from '../home/ButtonSimple';
import { LabelTitulo } from '../LabelTitulo';
import { Spacer } from '../Spacer';



export const Step4 = () => {

  const windowWidth = Dimensions.get('window').width;
  const { flags,setFlags } = useContext( GeneralContext )
   
   return <View style={{ flex:1,backgroundColor:'white',paddingTop:0, paddingHorizontal:8, justifyContent:'space-between',alignContent:'center',alignItems:'center' }}>
        
        <Spacer height={130}></Spacer>
        <View  ><Image style={{width:270,height:270}} source={require('../../assets/sinPreocupaciones.png')} ></Image></View>
       
            <LabelTitulo fontSizeTitulo={22} fontSizeSubtitulo={17} labelTitulo={'¡Sin preocupaciones!'} 
                         labelSubTitulo={'Un planner dara seguimiento personalizado a tu contratacion y resolvera todas tus dudas'} colorTitulo={'black'} colorSubTitulo={'black'} ></LabelTitulo>
           

            <View style={{alignItems:'center'}}>
                <ButtonSimple  backgroundColor='#110331' color='white' label={'Siguiente'} textDecorationLine={'none'} onPress={ ()=> {
                      console.log('siguiente home ')
                      const payload= flags;
                      payload.modalGuia01Visible = false;
                      setFlags(payload);

                } }></ButtonSimple>
                <Spacer height={10}></Spacer>

                <View style={{backgroundColor:'white', width:windowWidth,alignItems:'flex-end', right:-20}}>
                  <ButtonSimple  backgroundColor='transparent' color='black' label={'Omitir'} textDecorationLine={'underline'} onPress={ ()=> {
                        console.log('omitir')
                          const payload= flags;
                          payload.modalGuia01Visible = false;
                          setFlags(payload);
                  } }></ButtonSimple>
              </View>

              <Spacer height={50}></Spacer>
            </View>

        </View> 

}


//   const { flags,setFlags } = useContext( GeneralContext )
   
//    return <View style={{ flex:1,backgroundColor:'white',paddingTop:50, justifyContent:'center',alignContent:'center',alignItems:'center' }}>
//             <Text>Sin preocupaciones</Text>
//             <Spacer height={10}></Spacer>
//             <ButtonSimple  iconClose='ic_round-close' color='white' label={'Iniciar'} onPress={ ()=> {
//                   console.log('siguiente home ')
//                   const payload= flags;
//                   payload.modalGuia01Visible = false;
//                   setFlags(payload);
//             } }></ButtonSimple>
//             <Spacer height={10}></Spacer>
//             <ButtonSimple  iconClose='ic_round-close' color='white' label={'Omitir'} onPress={ ()=> {
//                   console.log('omitir')
//                     const payload= flags;
//                     payload.modalGuia01Visible = false;
//                     setFlags(payload);
//             } }></ButtonSimple>
//         </View> 

// }
