import React, { useContext, useState } from 'react';
import { Modal, View, Text, Platform, Button } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { gstyles } from '../../theme/appTheme';
import DateTimePicker from '@react-native-community/datetimepicker';
import { ComandoFiltro } from './ComandoFiltro';



export const ModalFecha = ( ) => {

  //call service to get data
  const { flags,setFlags,agendaFiltro,setAgendaFiltro } = useContext(GeneralContext);
  const [date, setDate] = useState(new Date());
  const [fechaAsignandaAux, setFechaAsignandaAux] = useState('');

  const add0AlMes =(messs :number) =>{
    switch (messs) {
    case 1:
      return '01'
      case 2:
      return '02'
      case  3:
      return '03'
      case  4:
      return '04'
      case  5:
      return '05'
      case  6:
      return '06'
      case  7:
      return '07'
      case  8:
      return '08'
      case  9:
      return '09'
    default:
      return messs.toString();
      break;
    }
}

  const onChange = (event:Event, selectedDate?:Date) => {
    const currentDate = selectedDate || date;

    
    //var auxDate = new Date(currentDate)
    //console.log(auxDate.toLocaleDateString('pt-BR'))
    //setDate(auxDate)
    //setDate(currentDate);

   

    //set date in human view
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = add0AlMes( currentDate.getMonth() + 1); //January is 0!
    var yyyy = currentDate.getFullYear();

    setFechaAsignandaAux(yyyy + '-' + mm + '-' + dd)
    setDate(new Date(yyyy + '-' + mm + '-' + dd))
     //set filtro
     var payload = agendaFiltro

     //console.log('fecha seteada::::')
     //console.log(yyyy + '-' + 'x'+mm+'x' + '-' + dd)
     payload.filtroFecha=(yyyy + '-' + mm +'-' + dd)
     setAgendaFiltro(payload)
  };



  const armaMes = (month:number) => {

    switch (month) {
      case 1:
        return 'Janeiro'
        case 2:
        return 'Fevereiro'
        case  3:
        return 'Março'
        case  4:
        return 'Abril'
        case  5:
        return 'Maio'
        case  6:
        return 'Junho'
        case  7:
        return 'Julho'
        case  8:
        return 'Agosto'
        case  9:
        return 'Setembro'
        case  10:
        return 'Outubro'
        case  11:
        return 'Novembro'
        case  12:
        return 'Dezembro'  
        case  13:
        return 'Dezembro'
      default:
        return '';
        break;
    }
  }


    return  <View>
             {/* fecha modal */}
              <Modal   animationType='fade' transparent={true}   visible={ flags.modalFechaVisible }>

                  <View style={{ flex:1,backgroundColor:'rgba(0,0,0,0.7)',justifyContent:'flex-start', paddingTop:190, alignItems:'center' }}>

                        <View style={{backgroundColor:'white',width:300,height:300,shadowOffset:{
                          width:0,height:10},shadowOpacity:0.27,elevation:4, borderRadius:6  }}>

                                <Text style={gstyles.tituloModal} >Escolha uma data</Text>

                                     <DateTimePicker 
                                          testID="dateTimePicker"
                                          // locale={'pt_BR'}
                                          value={date}
                                          mode={'date'}
                                          // is24Hour={true}
                                          display={Platform.OS=='ios' ? 'spinner': 'spinner'}
                                          onChange={onChange}
                                        />
                              
                                <ComandoFiltro label1='CANCEL' label2='OK'
                                  onPressAction1={()=>{
                                   //CANCEL
                                    const payload2= flags;
                                        payload2.modalFechaVisible =false;
                                        setFlags(payload2)

                                  }} onPressAction2={()=>{
                                    //OK
                                    console.log('ok sinseleccionar');
                                   
                                    if(fechaAsignandaAux=== '' ){
                                      console.log('generando fecha');
                                      var hoy = new Date()
                                      var dd = String(hoy.getDate()).padStart(2, '0');
                                      var mm = add0AlMes( hoy.getMonth() + 1); //January is 0!
                                      var yyyy = hoy.getFullYear();
                                  
                                      setFechaAsignandaAux(yyyy + '-' + mm + '-' + dd)
                                      const payload1 =agendaFiltro;
                                      payload1.filtroFecha = yyyy + '-' + mm + '-' + dd;
                                      setAgendaFiltro(payload1)
                                    }else{
                                      const payload1 =agendaFiltro;
                                      payload1.filtroFecha = fechaAsignandaAux;
                                      setAgendaFiltro(payload1)
                                    }
                                        const payload= flags;
                                        payload.modalFechaVisible =false;
                                        setFlags(payload)
                                  }} />

                        </View>
                     

                  </View>

              </Modal>
           </View>   
}
