
import React, { useContext } from 'react';
import { Text, View } from 'react-native';
import { GeneralContext } from '../../state/GeneralProvider';
import { HeaderTitle } from '../HeaderTitle';
import { Spacer } from '../Spacer';
import { ListOportunidades } from './ListOportunidades';

interface Props{
    height:number
  }

export const OportunidadesDia = ( ) => {

  const { agenda,setAgenda } = useContext( GeneralContext )

  const armaFecha = (fecha:string) => {
    let arrFecha= fecha.split('-');
    return arrFecha[2] +' de '+ armaMes(arrFecha[1])+ ' de '+ arrFecha[0] ;
  }

  const armaMes = (month:string) => {

    switch (month) {
      case '01':
        return 'Janeiro'
        case '02':
        return 'Fevereiro'
        case  '03':
        return 'Março'
        case  '04':
        return 'Abril'
        case  '05':
        return 'Maio'
        case  '06':
        return 'Junho'
        case  '07':
        return 'Julho'
        case  '08':
        return 'Agosto'
        case  '09':
        return 'Setembro'
        case  '10':
        return 'Outubro'
        case  '11':
        return 'Novembro'
        case  '12':
        return 'Dezembro'  
        case  '13':
        return 'Dezembro'
      default:
        return '';
        break;
    }
  }

  
  
    
    return  (<View style={{flex:1, backgroundColor:'#BCC1CB', top:40,  
                        width:'100%',   alignItems:'center',alignContent:'flex-start'}}>
                    <HeaderTitle label={armaFecha(agenda.selectedDate)} top={20} fontSize={18}></HeaderTitle>
                    <Spacer height={20}></Spacer>
     
                    { 
                      (agenda.markedDates[agenda.selectedDate] != undefined) ?
                      ( 
                        <ListOportunidades></ListOportunidades>
                      )
                    : (<View style={{flex:1,justifyContent:'flex-start',alignContent:'flex-start',marginTop:50,}}>
                      {/* <Spacer height={40}></Spacer> */}
                       <Text style={{fontFamily:'Roboto-Bold', fontSize:18,color:'#838892'}}>Sem eventos</Text>
                      </View>)
                    }
                   
              </View>
           )
}
