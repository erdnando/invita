import React from 'react';
import { Text, View, TextStyle, TouchableOpacity } from 'react-native';
import { useSearch } from '../hooks/useSearch';
import { Spacer } from './Spacer';

interface Props{
  fontSizeTitulo:number,
  fontSizeSubtitulo:number,
  labelTitulo:string,
  labelSubTitulo:string,
  colorTitulo:string,
  colorSubTitulo:string,
  separacion?:number,
  tituloMarginTop?:number,
  alineacion?:TextStyle['textAlign'],
}

export const LabelFiltroLimpiar = ( { fontSizeTitulo,fontSizeSubtitulo, labelTitulo,labelSubTitulo,colorTitulo,colorSubTitulo,separacion=8,alineacion='center',tituloMarginTop=0 }: Props ) => {
  const { applyFilters,applyOrder } = useSearch();
  const defaultValor=false;

     return <View style={{ flex:0,marginHorizontal:15,width:'95%',height:30, flexDirection:'row' }}>   

               <Text style={{height:25, fontFamily:'Roboto-Regular',marginTop:tituloMarginTop, fontSize:fontSizeTitulo,fontWeight:'700', textAlign:alineacion, marginBottom:0, color:colorTitulo}}>{labelTitulo}</Text>
               
              
               <TouchableOpacity activeOpacity={0.2}  style={{ 
                         borderRadius:1, shadowOpacity: 0.4,shadowOffset: {
                         height: 1, width: 1 },}} onPress={() =>{ 
             
                          applyFilters(defaultValor,defaultValor,defaultValor,defaultValor,defaultValor)
                          applyOrder(defaultValor,defaultValor,defaultValor,defaultValor)
                }}>
                  
                  <View style={{marginLeft:20,}}>
                    <Text style={{textDecorationLine:'underline', fontFamily:'Roboto-Regular',marginTop:0, fontSize:fontSizeSubtitulo,fontWeight:'400', textAlign:alineacion, color:colorSubTitulo}}>{labelSubTitulo}</Text>
                  </View>
                  
              </TouchableOpacity>


        
            </View>
          
}
